// Compiled by ClojureScript 0.0-2227
goog.provide('ajax.core');
goog.require('cljs.core');
goog.require('goog.Uri');
goog.require('goog.net.XhrIo');
goog.require('goog.net.XhrManager');
goog.require('goog.net.XhrIo');
goog.require('goog.Uri.QueryData');
goog.require('goog.Uri');
goog.require('goog.Uri.QueryData');
goog.require('goog.net.EventType');
goog.require('goog.events');
goog.require('goog.structs');
goog.require('goog.structs');
goog.require('goog.json.Serializer');
goog.require('goog.net.XhrManager');
goog.require('clojure.string');
goog.require('clojure.string');
goog.require('cljs.reader');
goog.require('goog.events');
goog.require('cljs.reader');
goog.require('goog.net.ErrorCode');
ajax.core.AjaxImpl = (function (){var obj9503 = {};return obj9503;
})();
ajax.core._js_ajax_request = (function _js_ajax_request(this$,uri,method,body,headers,handler,opts){if((function (){var and__3457__auto__ = this$;if(and__3457__auto__)
{return this$.ajax$core$AjaxImpl$_js_ajax_request$arity$7;
} else
{return and__3457__auto__;
}
})())
{return this$.ajax$core$AjaxImpl$_js_ajax_request$arity$7(this$,uri,method,body,headers,handler,opts);
} else
{var x__4096__auto__ = (((this$ == null))?null:this$);return (function (){var or__3469__auto__ = (ajax.core._js_ajax_request[goog.typeOf(x__4096__auto__)]);if(or__3469__auto__)
{return or__3469__auto__;
} else
{var or__3469__auto____$1 = (ajax.core._js_ajax_request["_"]);if(or__3469__auto____$1)
{return or__3469__auto____$1;
} else
{throw cljs.core.missing_protocol("AjaxImpl.-js-ajax-request",this$);
}
}
})().call(null,this$,uri,method,body,headers,handler,opts);
}
});
ajax.core.AjaxRequest = (function (){var obj9505 = {};return obj9505;
})();
ajax.core._abort = (function _abort(this$,error_code){if((function (){var and__3457__auto__ = this$;if(and__3457__auto__)
{return this$.ajax$core$AjaxRequest$_abort$arity$2;
} else
{return and__3457__auto__;
}
})())
{return this$.ajax$core$AjaxRequest$_abort$arity$2(this$,error_code);
} else
{var x__4096__auto__ = (((this$ == null))?null:this$);return (function (){var or__3469__auto__ = (ajax.core._abort[goog.typeOf(x__4096__auto__)]);if(or__3469__auto__)
{return or__3469__auto__;
} else
{var or__3469__auto____$1 = (ajax.core._abort["_"]);if(or__3469__auto____$1)
{return or__3469__auto____$1;
} else
{throw cljs.core.missing_protocol("AjaxRequest.-abort",this$);
}
}
})().call(null,this$,error_code);
}
});
(ajax.core.AjaxImpl["null"] = true);
(ajax.core._js_ajax_request["null"] = (function (this$,uri,method,body,headers,handler,p__9506){var map__9507 = p__9506;var map__9507__$1 = ((cljs.core.seq_QMARK_(map__9507))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__9507):map__9507);var timeout = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9507__$1,cljs.core.constant$keyword$189);var G__9508 = (new goog.net.XhrIo());goog.events.listen(G__9508,goog.net.EventType.COMPLETE,handler);
G__9508.setTimeoutInterval((function (){var or__3469__auto__ = timeout;if(cljs.core.truth_(or__3469__auto__))
{return or__3469__auto__;
} else
{return 0;
}
})());
G__9508.send(uri,method,body,headers);
return G__9508;
}));
goog.net.XhrIo.prototype.ajax$core$AjaxRequest$ = true;
goog.net.XhrIo.prototype.ajax$core$AjaxRequest$_abort$arity$2 = (function (this$,error_code){var this$__$1 = this;return this$__$1.abort(error_code);
});
goog.net.XhrManager.prototype.ajax$core$AjaxImpl$ = true;
goog.net.XhrManager.prototype.ajax$core$AjaxImpl$_js_ajax_request$arity$7 = (function (this$,uri,method,body,headers,handler,p__9509){var map__9510 = p__9509;var map__9510__$1 = ((cljs.core.seq_QMARK_(map__9510))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__9510):map__9510);var max_retries = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9510__$1,cljs.core.constant$keyword$190);var priority = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9510__$1,cljs.core.constant$keyword$191);var timeout = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9510__$1,cljs.core.constant$keyword$189);var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9510__$1,cljs.core.constant$keyword$192);var this$__$1 = this;return this$__$1.send(id,uri,method,body,headers,priority,handler,max_retries);
});
ajax.core.abort = (function abort(this$){return ajax.core._abort(this$,goog.net.ErrorCode.ABORT);
});
ajax.core.success_QMARK_ = (function success_QMARK_(status){return cljs.core.some(cljs.core.PersistentHashSet.fromArray([status], true),new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [200,201,202,204,205,206], null));
});
ajax.core.read_edn = (function read_edn(xhrio){return cljs.reader.read_string(xhrio.getResponseText());
});
ajax.core.edn_response_format = (function edn_response_format(){return new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$193,ajax.core.read_edn,cljs.core.constant$keyword$194,"EDN"], null);
});
ajax.core.edn_request_format = (function edn_request_format(){return new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$195,cljs.core.pr_str,cljs.core.constant$keyword$196,"application/edn"], null);
});
ajax.core.params_to_str = (function params_to_str(params){if(cljs.core.truth_(params))
{return goog.Uri.QueryData.createFromMap((new goog.structs.Map(cljs.core.clj__GT_js(params)))).toString();
} else
{return null;
}
});
ajax.core.read_text = (function read_text(xhrio){return xhrio.getResponseText();
});
ajax.core.url_request_format = (function url_request_format(){return new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$195,ajax.core.params_to_str,cljs.core.constant$keyword$196,"application/x-www-form-urlencoded"], null);
});
ajax.core.raw_response_format = (function raw_response_format(){return new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$193,ajax.core.read_text,cljs.core.constant$keyword$194,"raw text"], null);
});
ajax.core.write_json = (function write_json(data){return (new goog.json.Serializer()).serialize(cljs.core.clj__GT_js(data));
});
ajax.core.json_request_format = (function json_request_format(){return new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$195,ajax.core.write_json,cljs.core.constant$keyword$196,"application/json"], null);
});
/**
* Returns a JSON response format.  Options include
* :keywords? Returns the keys as keywords
* :prefix A prefix that needs to be stripped off.  This is to
* combat JSON hijacking.  If you're using JSON with GET request,
* you should use this.
* http://stackoverflow.com/questions/2669690/why-does-google-prepend-while1-to-their-json-responses
* http://haacked.com/archive/2009/06/24/json-hijacking.aspx
*/
ajax.core.json_response_format = (function json_response_format(p__9511){var map__9513 = p__9511;var map__9513__$1 = ((cljs.core.seq_QMARK_(map__9513))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__9513):map__9513);var keywords_QMARK_ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9513__$1,cljs.core.constant$keyword$197);var prefix = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9513__$1,cljs.core.constant$keyword$198);return new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$193,((function (map__9513,map__9513__$1,keywords_QMARK_,prefix){
return (function read_json(xhrio){var json = xhrio.getResponseJson(prefix);return cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$variadic(json,cljs.core.array_seq([cljs.core.constant$keyword$184,keywords_QMARK_], 0));
});})(map__9513,map__9513__$1,keywords_QMARK_,prefix))
,cljs.core.constant$keyword$194,("JSON"+cljs.core.str.cljs$core$IFn$_invoke$arity$1((cljs.core.truth_(prefix)?(" prefix '"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(prefix)+"'"):null))+cljs.core.str.cljs$core$IFn$_invoke$arity$1((cljs.core.truth_(keywords_QMARK_)?" keywordize":null)))], null);
});
ajax.core.get_default_format = (function get_default_format(xhrio){var ct = xhrio.getResponseHeader("Content-Type");var format = (cljs.core.truth_((function (){var and__3457__auto__ = ct;if(cljs.core.truth_(and__3457__auto__))
{return (ct.indexOf("json") >= 0);
} else
{return and__3457__auto__;
}
})())?ajax.core.json_response_format(cljs.core.PersistentArrayMap.EMPTY):ajax.core.edn_response_format());return cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(format,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$194], null),((function (ct,format){
return (function (p1__9514_SHARP_){return (''+cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__9514_SHARP_)+" (default)");
});})(ct,format))
);
});
ajax.core.use_content_type = (function use_content_type(format){return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(format,cljs.core.constant$keyword$195);
});
ajax.core.get_format = (function get_format(format){if(cljs.core.map_QMARK_(format))
{return format;
} else
{if(cljs.core.ifn_QMARK_(format))
{return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([ajax.core.url_request_format(),new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$193,format,cljs.core.constant$keyword$194,"custom"], null)], 0));
} else
{if(cljs.core.constant$keyword$178)
{throw (new Error(("unrecognized format: "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(format))));
} else
{return null;
}
}
}
});
ajax.core.exception_response = (function exception_response(e,status,p__9515,xhrio){var map__9517 = p__9515;var map__9517__$1 = ((cljs.core.seq_QMARK_(map__9517))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__9517):map__9517);var description = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9517__$1,cljs.core.constant$keyword$194);var response = new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$199,status,cljs.core.constant$keyword$200,null], null);var status_text = (''+cljs.core.str.cljs$core$IFn$_invoke$arity$1(e.message)+"  Format should have been "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(description));var parse_error = cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(response,cljs.core.constant$keyword$201,status_text,cljs.core.array_seq([cljs.core.constant$keyword$202,true,cljs.core.constant$keyword$203,xhrio.getResponseText()], 0));if(cljs.core.truth_(ajax.core.success_QMARK_(status)))
{return parse_error;
} else
{return cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(response,cljs.core.constant$keyword$201,xhrio.getStatusText(),cljs.core.array_seq([cljs.core.constant$keyword$204,parse_error], 0));
}
});
ajax.core.interpret_response = (function interpret_response(format,response,get_default_format){try{var xhrio = response.target;var status = xhrio.getStatus();if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(-1,status))
{if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(xhrio.getLastErrorCode(),goog.net.ErrorCode.ABORT))
{return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [false,new cljs.core.PersistentArrayMap(null, 3, [cljs.core.constant$keyword$199,-1,cljs.core.constant$keyword$201,"Request aborted by client.",cljs.core.constant$keyword$205,true], null)], null);
} else
{return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [false,new cljs.core.PersistentArrayMap(null, 3, [cljs.core.constant$keyword$199,-1,cljs.core.constant$keyword$201,"Request timed out.",cljs.core.constant$keyword$206,true], null)], null);
}
} else
{var format__$1 = (cljs.core.truth_(cljs.core.constant$keyword$193.cljs$core$IFn$_invoke$arity$1(format))?format:(get_default_format.cljs$core$IFn$_invoke$arity$1 ? get_default_format.cljs$core$IFn$_invoke$arity$1(xhrio) : get_default_format.call(null,xhrio)));var parse = cljs.core.constant$keyword$193.cljs$core$IFn$_invoke$arity$1(format__$1);try{var response__$1 = (parse.cljs$core$IFn$_invoke$arity$1 ? parse.cljs$core$IFn$_invoke$arity$1(xhrio) : parse.call(null,xhrio));if(cljs.core.truth_(ajax.core.success_QMARK_(status)))
{return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [true,response__$1], null);
} else
{return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [false,new cljs.core.PersistentArrayMap(null, 3, [cljs.core.constant$keyword$199,status,cljs.core.constant$keyword$201,xhrio.getStatusText(),cljs.core.constant$keyword$200,response__$1], null)], null);
}
}catch (e9521){if((e9521 instanceof Object))
{var e = e9521;return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [false,ajax.core.exception_response(e,status,format__$1,xhrio)], null);
} else
{if(cljs.core.constant$keyword$178)
{throw e9521;
} else
{return null;
}
}
}}
}catch (e9520){if((e9520 instanceof Object))
{var e = e9520;return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [false,new cljs.core.PersistentArrayMap(null, 3, [cljs.core.constant$keyword$199,0,cljs.core.constant$keyword$201,e.message,cljs.core.constant$keyword$200,null], null)], null);
} else
{if(cljs.core.constant$keyword$178)
{throw e9520;
} else
{return null;
}
}
}});
ajax.core.no_format = (function no_format(xhrio){throw (new Error("No response format was supplied."));
});
ajax.core.uri_with_params = (function uri_with_params(uri,params){if(cljs.core.truth_(params))
{return (''+cljs.core.str.cljs$core$IFn$_invoke$arity$1(uri)+"?"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(ajax.core.params_to_str(params)));
} else
{return uri;
}
});
ajax.core.process_inputs = (function process_inputs(uri,method,p__9522,p__9523){var map__9527 = p__9522;var map__9527__$1 = ((cljs.core.seq_QMARK_(map__9527))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__9527):map__9527);var format = map__9527__$1;var content_type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9527__$1,cljs.core.constant$keyword$196);var write = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9527__$1,cljs.core.constant$keyword$195);var map__9528 = p__9523;var map__9528__$1 = ((cljs.core.seq_QMARK_(map__9528))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__9528):map__9528);var headers = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9528__$1,cljs.core.constant$keyword$207);var params = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9528__$1,cljs.core.constant$keyword$208);if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(method,"GET"))
{return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [ajax.core.uri_with_params(uri,params),null,headers], null);
} else
{var map__9529 = format;var map__9529__$1 = ((cljs.core.seq_QMARK_(map__9529))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__9529):map__9529);var content_type__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9529__$1,cljs.core.constant$keyword$196);var write__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9529__$1,cljs.core.constant$keyword$195);var body = (write__$1.cljs$core$IFn$_invoke$arity$1 ? write__$1.cljs$core$IFn$_invoke$arity$1(params) : write__$1.call(null,params));var content_type__$2 = (cljs.core.truth_(content_type__$1)?new cljs.core.PersistentArrayMap(null, 1, ["Content-Type",content_type__$1], null):null);var headers__$1 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([(function (){var or__3469__auto__ = headers;if(cljs.core.truth_(or__3469__auto__))
{return or__3469__auto__;
} else
{return cljs.core.PersistentArrayMap.EMPTY;
}
})(),content_type__$2], 0));return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [uri,body,headers__$1], null);
}
});
ajax.core.normalize_method = (function normalize_method(method){if((method instanceof cljs.core.Keyword))
{return clojure.string.upper_case(cljs.core.name(method));
} else
{return method;
}
});
ajax.core.base_handler = (function base_handler(format,p__9530){var map__9532 = p__9530;var map__9532__$1 = ((cljs.core.seq_QMARK_(map__9532))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__9532):map__9532);var get_default_format = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9532__$1,cljs.core.constant$keyword$209);var handler = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9532__$1,cljs.core.constant$keyword$210);if(cljs.core.truth_(handler))
{return ((function (map__9532,map__9532__$1,get_default_format,handler){
return (function (xhrio){return (handler.cljs$core$IFn$_invoke$arity$1 ? handler.cljs$core$IFn$_invoke$arity$1(ajax.core.interpret_response(format,xhrio,(function (){var or__3469__auto__ = get_default_format;if(cljs.core.truth_(or__3469__auto__))
{return or__3469__auto__;
} else
{return ajax.core.no_format;
}
})())) : handler.call(null,ajax.core.interpret_response(format,xhrio,(function (){var or__3469__auto__ = get_default_format;if(cljs.core.truth_(or__3469__auto__))
{return or__3469__auto__;
} else
{return ajax.core.no_format;
}
})())));
});
;})(map__9532,map__9532__$1,get_default_format,handler))
} else
{throw (new Error("No ajax handler provided."));
}
});
/**
* @param {...*} var_args
*/
ajax.core.ajax_request = (function() {
var ajax_request = null;
var ajax_request__1 = (function (p__9533){var map__9536 = p__9533;var map__9536__$1 = ((cljs.core.seq_QMARK_(map__9536))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__9536):map__9536);var opts = map__9536__$1;var manager = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9536__$1,cljs.core.constant$keyword$211);var format = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9536__$1,cljs.core.constant$keyword$212);var method = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9536__$1,cljs.core.constant$keyword$213);var uri = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9536__$1,cljs.core.constant$keyword$214);var format__$1 = ajax.core.get_format(format);var method__$1 = ajax.core.normalize_method(method);var vec__9537 = ajax.core.process_inputs(uri,method__$1,format__$1,opts);var uri__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__9537,0,null);var body = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__9537,1,null);var headers = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__9537,2,null);var handler = ajax.core.base_handler(format__$1,opts);return ajax.core._js_ajax_request(manager,uri__$1,method__$1,body,cljs.core.clj__GT_js(headers),handler,opts);
});
var ajax_request__3 = (function() { 
var G__9538__delegate = function (uri,method,args){var f = cljs.core.first(args);var opts = (((f instanceof cljs.core.Keyword))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,args):f);return ajax_request.cljs$core$IFn$_invoke$arity$1(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(opts,cljs.core.constant$keyword$214,uri,cljs.core.array_seq([cljs.core.constant$keyword$213,method], 0)));
};
var G__9538 = function (uri,method,var_args){
var args = null;if (arguments.length > 2) {
  args = cljs.core.array_seq(Array.prototype.slice.call(arguments, 2),0);} 
return G__9538__delegate.call(this,uri,method,args);};
G__9538.cljs$lang$maxFixedArity = 2;
G__9538.cljs$lang$applyTo = (function (arglist__9539){
var uri = cljs.core.first(arglist__9539);
arglist__9539 = cljs.core.next(arglist__9539);
var method = cljs.core.first(arglist__9539);
var args = cljs.core.rest(arglist__9539);
return G__9538__delegate(uri,method,args);
});
G__9538.cljs$core$IFn$_invoke$arity$variadic = G__9538__delegate;
return G__9538;
})()
;
ajax_request = function(uri,method,var_args){
var args = var_args;
switch(arguments.length){
case 1:
return ajax_request__1.call(this,uri);
default:
return ajax_request__3.cljs$core$IFn$_invoke$arity$variadic(uri,method, cljs.core.array_seq(arguments, 2));
}
throw(new Error('Invalid arity: ' + arguments.length));
};
ajax_request.cljs$lang$maxFixedArity = 2;
ajax_request.cljs$lang$applyTo = ajax_request__3.cljs$lang$applyTo;
ajax_request.cljs$core$IFn$_invoke$arity$1 = ajax_request__1;
ajax_request.cljs$core$IFn$_invoke$arity$variadic = ajax_request__3.cljs$core$IFn$_invoke$arity$variadic;
return ajax_request;
})()
;
ajax.core.json_format = (function json_format(format_params){return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([ajax.core.json_request_format(),ajax.core.json_response_format(format_params)], 0));
});
ajax.core.edn_format = (function edn_format(){return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([ajax.core.edn_request_format(),ajax.core.edn_response_format()], 0));
});
ajax.core.raw_format = (function raw_format(){return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([ajax.core.url_request_format(),ajax.core.raw_response_format()], 0));
});
ajax.core.keyword_request_format = (function keyword_request_format(format,format_params){var G__9541 = (((format instanceof cljs.core.Keyword))?format.fqn:null);var caseval__9542;
switch (G__9541){
case "url":
caseval__9542=ajax.core.url_request_format()
break;
case "raw":
caseval__9542=ajax.core.url_request_format()
break;
case "edn":
caseval__9542=ajax.core.edn_request_format()
break;
case "json":
caseval__9542=ajax.core.json_request_format()
break;
default:
caseval__9542=(function(){throw (new Error(("unrecognized request format: "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(format))))})()
}
return caseval__9542;
});
ajax.core.keyword_response_format = (function keyword_response_format(format,format_params){if(cljs.core.map_QMARK_(format))
{return format;
} else
{if(cljs.core.ifn_QMARK_(format))
{return new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$193,format,cljs.core.constant$keyword$194,"custom"], null);
} else
{if(cljs.core.constant$keyword$178)
{var G__9544 = (((format instanceof cljs.core.Keyword))?format.fqn:null);var caseval__9545;
switch (G__9544){
case "raw":
caseval__9545=ajax.core.raw_response_format()
break;
case "edn":
caseval__9545=ajax.core.edn_response_format()
break;
case "json":
caseval__9545=ajax.core.json_response_format(format_params)
break;
default:
caseval__9545=null
}
return caseval__9545;
} else
{return null;
}
}
}
});
ajax.core.transform_handler = (function transform_handler(p__9546){var map__9551 = p__9546;var map__9551__$1 = ((cljs.core.seq_QMARK_(map__9551))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__9551):map__9551);var finally$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9551__$1,cljs.core.constant$keyword$215);var error_handler = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9551__$1,cljs.core.constant$keyword$216);var handler = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9551__$1,cljs.core.constant$keyword$210);return ((function (map__9551,map__9551__$1,finally$,error_handler,handler){
return (function easy_handler(p__9552){var vec__9554 = p__9552;var ok = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__9554,0,null);var result = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__9554,1,null);var temp__4124__auto___9555 = (cljs.core.truth_(ok)?handler:error_handler);if(cljs.core.truth_(temp__4124__auto___9555))
{var h_9556 = temp__4124__auto___9555;(h_9556.cljs$core$IFn$_invoke$arity$1 ? h_9556.cljs$core$IFn$_invoke$arity$1(result) : h_9556.call(null,result));
} else
{}
if(cljs.core.fn_QMARK_(finally$))
{return (finally$.cljs$core$IFn$_invoke$arity$0 ? finally$.cljs$core$IFn$_invoke$arity$0() : finally$.call(null));
} else
{return null;
}
});
;})(map__9551,map__9551__$1,finally$,error_handler,handler))
});
ajax.core.transform_format = (function transform_format(p__9557){var map__9559 = p__9557;var map__9559__$1 = ((cljs.core.seq_QMARK_(map__9559))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__9559):map__9559);var opts = map__9559__$1;var response_format = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9559__$1,cljs.core.constant$keyword$217);var format = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9559__$1,cljs.core.constant$keyword$212);var rf = ajax.core.keyword_response_format(response_format,opts);if((format == null))
{return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([ajax.core.edn_request_format(),rf], 0));
} else
{if((format instanceof cljs.core.Keyword))
{return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([ajax.core.keyword_request_format(format,opts),rf], 0));
} else
{if(cljs.core.constant$keyword$178)
{return format;
} else
{return null;
}
}
}
});
ajax.core.transform_opts = (function transform_opts(opts){return cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(opts,cljs.core.constant$keyword$210,ajax.core.transform_handler(opts),cljs.core.array_seq([cljs.core.constant$keyword$212,ajax.core.transform_format(opts),cljs.core.constant$keyword$209,ajax.core.get_default_format], 0));
});
/**
* accepts the URI and an optional map of options, options include:
* :handler - the handler function for successful operation
* should accept a single parameter which is the
* deserialized response
* :error-handler - the handler function for errors, should accept a
* map with keys :status and :status-text
* :format - the format for the request
* :response-format - the format for the response
* :params - a map of parameters that will be sent with the request
* @param {...*} var_args
*/
ajax.core.GET = (function() { 
var GET__delegate = function (uri,p__9560){var vec__9562 = p__9560;var opts = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__9562,0,null);return ajax.core.ajax_request.cljs$core$IFn$_invoke$arity$variadic(uri,"GET",cljs.core.array_seq([ajax.core.transform_opts(opts)], 0));
};
var GET = function (uri,var_args){
var p__9560 = null;if (arguments.length > 1) {
  p__9560 = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);} 
return GET__delegate.call(this,uri,p__9560);};
GET.cljs$lang$maxFixedArity = 1;
GET.cljs$lang$applyTo = (function (arglist__9563){
var uri = cljs.core.first(arglist__9563);
var p__9560 = cljs.core.rest(arglist__9563);
return GET__delegate(uri,p__9560);
});
GET.cljs$core$IFn$_invoke$arity$variadic = GET__delegate;
return GET;
})()
;
/**
* accepts the URI and an optional map of options, options include:
* :handler - the handler function for successful operation
* should accept a single parameter which is the
* deserialized response
* :error-handler - the handler function for errors, should accept a
* map with keys :status and :status-text
* :format - the format for the request
* :response-format - the format for the response
* :params - a map of parameters that will be sent with the request
* @param {...*} var_args
*/
ajax.core.HEAD = (function() { 
var HEAD__delegate = function (uri,p__9564){var vec__9566 = p__9564;var opts = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__9566,0,null);return ajax.core.ajax_request.cljs$core$IFn$_invoke$arity$variadic(uri,"HEAD",cljs.core.array_seq([ajax.core.transform_opts(opts)], 0));
};
var HEAD = function (uri,var_args){
var p__9564 = null;if (arguments.length > 1) {
  p__9564 = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);} 
return HEAD__delegate.call(this,uri,p__9564);};
HEAD.cljs$lang$maxFixedArity = 1;
HEAD.cljs$lang$applyTo = (function (arglist__9567){
var uri = cljs.core.first(arglist__9567);
var p__9564 = cljs.core.rest(arglist__9567);
return HEAD__delegate(uri,p__9564);
});
HEAD.cljs$core$IFn$_invoke$arity$variadic = HEAD__delegate;
return HEAD;
})()
;
/**
* accepts the URI and an optional map of options, options include:
* :handler - the handler function for successful operation
* should accept a single parameter which is the
* deserialized response
* :error-handler - the handler function for errors, should accept a
* map with keys :status and :status-text
* :format - the format for the request
* :response-format - the format for the response
* :params - a map of parameters that will be sent with the request
* @param {...*} var_args
*/
ajax.core.POST = (function() { 
var POST__delegate = function (uri,p__9568){var vec__9570 = p__9568;var opts = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__9570,0,null);return ajax.core.ajax_request.cljs$core$IFn$_invoke$arity$variadic(uri,"POST",cljs.core.array_seq([ajax.core.transform_opts(opts)], 0));
};
var POST = function (uri,var_args){
var p__9568 = null;if (arguments.length > 1) {
  p__9568 = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);} 
return POST__delegate.call(this,uri,p__9568);};
POST.cljs$lang$maxFixedArity = 1;
POST.cljs$lang$applyTo = (function (arglist__9571){
var uri = cljs.core.first(arglist__9571);
var p__9568 = cljs.core.rest(arglist__9571);
return POST__delegate(uri,p__9568);
});
POST.cljs$core$IFn$_invoke$arity$variadic = POST__delegate;
return POST;
})()
;
/**
* accepts the URI and an optional map of options, options include:
* :handler - the handler function for successful operation
* should accept a single parameter which is the
* deserialized response
* :error-handler - the handler function for errors, should accept a
* map with keys :status and :status-text
* :format - the format for the request
* :response-format - the format for the response
* :params - a map of parameters that will be sent with the request
* @param {...*} var_args
*/
ajax.core.PUT = (function() { 
var PUT__delegate = function (uri,p__9572){var vec__9574 = p__9572;var opts = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__9574,0,null);return ajax.core.ajax_request.cljs$core$IFn$_invoke$arity$variadic(uri,"PUT",cljs.core.array_seq([ajax.core.transform_opts(opts)], 0));
};
var PUT = function (uri,var_args){
var p__9572 = null;if (arguments.length > 1) {
  p__9572 = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);} 
return PUT__delegate.call(this,uri,p__9572);};
PUT.cljs$lang$maxFixedArity = 1;
PUT.cljs$lang$applyTo = (function (arglist__9575){
var uri = cljs.core.first(arglist__9575);
var p__9572 = cljs.core.rest(arglist__9575);
return PUT__delegate(uri,p__9572);
});
PUT.cljs$core$IFn$_invoke$arity$variadic = PUT__delegate;
return PUT;
})()
;
/**
* accepts the URI and an optional map of options, options include:
* :handler - the handler function for successful operation
* should accept a single parameter which is the
* deserialized response
* :error-handler - the handler function for errors, should accept a
* map with keys :status and :status-text
* :format - the format for the request
* :response-format - the format for the response
* :params - a map of parameters that will be sent with the request
* @param {...*} var_args
*/
ajax.core.DELETE = (function() { 
var DELETE__delegate = function (uri,p__9576){var vec__9578 = p__9576;var opts = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__9578,0,null);return ajax.core.ajax_request.cljs$core$IFn$_invoke$arity$variadic(uri,"DELETE",cljs.core.array_seq([ajax.core.transform_opts(opts)], 0));
};
var DELETE = function (uri,var_args){
var p__9576 = null;if (arguments.length > 1) {
  p__9576 = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);} 
return DELETE__delegate.call(this,uri,p__9576);};
DELETE.cljs$lang$maxFixedArity = 1;
DELETE.cljs$lang$applyTo = (function (arglist__9579){
var uri = cljs.core.first(arglist__9579);
var p__9576 = cljs.core.rest(arglist__9579);
return DELETE__delegate(uri,p__9576);
});
DELETE.cljs$core$IFn$_invoke$arity$variadic = DELETE__delegate;
return DELETE;
})()
;
/**
* accepts the URI and an optional map of options, options include:
* :handler - the handler function for successful operation
* should accept a single parameter which is the
* deserialized response
* :error-handler - the handler function for errors, should accept a
* map with keys :status and :status-text
* :format - the format for the request
* :response-format - the format for the response
* :params - a map of parameters that will be sent with the request
* @param {...*} var_args
*/
ajax.core.OPTIONS = (function() { 
var OPTIONS__delegate = function (uri,p__9580){var vec__9582 = p__9580;var opts = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__9582,0,null);return ajax.core.ajax_request.cljs$core$IFn$_invoke$arity$variadic(uri,"OPTIONS",cljs.core.array_seq([ajax.core.transform_opts(opts)], 0));
};
var OPTIONS = function (uri,var_args){
var p__9580 = null;if (arguments.length > 1) {
  p__9580 = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);} 
return OPTIONS__delegate.call(this,uri,p__9580);};
OPTIONS.cljs$lang$maxFixedArity = 1;
OPTIONS.cljs$lang$applyTo = (function (arglist__9583){
var uri = cljs.core.first(arglist__9583);
var p__9580 = cljs.core.rest(arglist__9583);
return OPTIONS__delegate(uri,p__9580);
});
OPTIONS.cljs$core$IFn$_invoke$arity$variadic = OPTIONS__delegate;
return OPTIONS;
})()
;
/**
* accepts the URI and an optional map of options, options include:
* :handler - the handler function for successful operation
* should accept a single parameter which is the
* deserialized response
* :error-handler - the handler function for errors, should accept a
* map with keys :status and :status-text
* :format - the format for the request
* :response-format - the format for the response
* :params - a map of parameters that will be sent with the request
* @param {...*} var_args
*/
ajax.core.TRACE = (function() { 
var TRACE__delegate = function (uri,p__9584){var vec__9586 = p__9584;var opts = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__9586,0,null);return ajax.core.ajax_request.cljs$core$IFn$_invoke$arity$variadic(uri,"TRACE",cljs.core.array_seq([ajax.core.transform_opts(opts)], 0));
};
var TRACE = function (uri,var_args){
var p__9584 = null;if (arguments.length > 1) {
  p__9584 = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);} 
return TRACE__delegate.call(this,uri,p__9584);};
TRACE.cljs$lang$maxFixedArity = 1;
TRACE.cljs$lang$applyTo = (function (arglist__9587){
var uri = cljs.core.first(arglist__9587);
var p__9584 = cljs.core.rest(arglist__9587);
return TRACE__delegate(uri,p__9584);
});
TRACE.cljs$core$IFn$_invoke$arity$variadic = TRACE__delegate;
return TRACE;
})()
;
