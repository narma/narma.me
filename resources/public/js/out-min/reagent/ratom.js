// Compiled by ClojureScript 0.0-2227
goog.provide('reagent.ratom');
goog.require('cljs.core');
reagent.ratom.debug = false;
reagent.ratom._running = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(0);
reagent.ratom.running = (function running(){return cljs.core.deref(reagent.ratom._running);
});
reagent.ratom.capture_derefed = (function capture_derefed(f,obj){obj.cljsCaptured = null;
var _STAR_ratom_context_STAR_9759 = reagent.ratom._STAR_ratom_context_STAR_;try{reagent.ratom._STAR_ratom_context_STAR_ = obj;
return (f.cljs$core$IFn$_invoke$arity$0 ? f.cljs$core$IFn$_invoke$arity$0() : f.call(null));
}finally {reagent.ratom._STAR_ratom_context_STAR_ = _STAR_ratom_context_STAR_9759;
}});
reagent.ratom.captured = (function captured(obj){var c = obj.cljsCaptured;obj.cljsCaptured = null;
return c;
});
reagent.ratom.notify_deref_watcher_BANG_ = (function notify_deref_watcher_BANG_(derefable){var obj = reagent.ratom._STAR_ratom_context_STAR_;if((obj == null))
{return null;
} else
{var captured = obj.cljsCaptured;return obj.cljsCaptured = cljs.core.conj.cljs$core$IFn$_invoke$arity$2((((captured == null))?cljs.core.PersistentHashSet.EMPTY:captured),derefable);
}
});

/**
* @constructor
*/
reagent.ratom.RAtom = (function (state,meta,validator,watches){
this.state = state;
this.meta = meta;
this.validator = validator;
this.watches = watches;
this.cljs$lang$protocol_mask$partition0$ = 2153938944;
this.cljs$lang$protocol_mask$partition1$ = 114690;
})
reagent.ratom.RAtom.cljs$lang$type = true;
reagent.ratom.RAtom.cljs$lang$ctorStr = "reagent.ratom/RAtom";
reagent.ratom.RAtom.cljs$lang$ctorPrWriter = (function (this__4036__auto__,writer__4037__auto__,opt__4038__auto__){return cljs.core._write(writer__4037__auto__,"reagent.ratom/RAtom");
});
reagent.ratom.RAtom.prototype.cljs$core$IHash$_hash$arity$1 = (function (this$){var self__ = this;
var this$__$1 = this;return goog.getUid(this$__$1);
});
reagent.ratom.RAtom.prototype.cljs$core$IWatchable$_notify_watches$arity$3 = (function (this$,oldval,newval){var self__ = this;
var this$__$1 = this;return cljs.core.reduce_kv(((function (this$__$1){
return (function (_,key,f){(f.cljs$core$IFn$_invoke$arity$4 ? f.cljs$core$IFn$_invoke$arity$4(key,this$__$1,oldval,newval) : f.call(null,key,this$__$1,oldval,newval));
return null;
});})(this$__$1))
,null,self__.watches);
});
reagent.ratom.RAtom.prototype.cljs$core$IWatchable$_add_watch$arity$3 = (function (this$,key,f){var self__ = this;
var this$__$1 = this;return self__.watches = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.watches,key,f);
});
reagent.ratom.RAtom.prototype.cljs$core$IWatchable$_remove_watch$arity$2 = (function (this$,key){var self__ = this;
var this$__$1 = this;return self__.watches = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.watches,key);
});
reagent.ratom.RAtom.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (a,writer,opts){var self__ = this;
var a__$1 = this;cljs.core._write(writer,"#<Atom: ");
cljs.core.pr_writer(self__.state,writer,opts);
return cljs.core._write(writer,">");
});
reagent.ratom.RAtom.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return self__.meta;
});
reagent.ratom.RAtom.prototype.cljs$core$ISwap$_swap_BANG_$arity$2 = (function (a,f){var self__ = this;
var a__$1 = this;return cljs.core._reset_BANG_(a__$1,(f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(self__.state) : f.call(null,self__.state)));
});
reagent.ratom.RAtom.prototype.cljs$core$ISwap$_swap_BANG_$arity$3 = (function (a,f,x){var self__ = this;
var a__$1 = this;return cljs.core._reset_BANG_(a__$1,(f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(self__.state,x) : f.call(null,self__.state,x)));
});
reagent.ratom.RAtom.prototype.cljs$core$ISwap$_swap_BANG_$arity$4 = (function (a,f,x,y){var self__ = this;
var a__$1 = this;return cljs.core._reset_BANG_(a__$1,(f.cljs$core$IFn$_invoke$arity$3 ? f.cljs$core$IFn$_invoke$arity$3(self__.state,x,y) : f.call(null,self__.state,x,y)));
});
reagent.ratom.RAtom.prototype.cljs$core$ISwap$_swap_BANG_$arity$5 = (function (a,f,x,y,more){var self__ = this;
var a__$1 = this;return cljs.core._reset_BANG_(a__$1,cljs.core.apply.cljs$core$IFn$_invoke$arity$5(f,self__.state,x,y,more));
});
reagent.ratom.RAtom.prototype.cljs$core$IReset$_reset_BANG_$arity$2 = (function (a,new_value){var self__ = this;
var a__$1 = this;if((self__.validator == null))
{} else
{}
var old_value = self__.state;self__.state = new_value;
if((self__.watches == null))
{} else
{cljs.core._notify_watches(a__$1,old_value,new_value);
}
return new_value;
});
reagent.ratom.RAtom.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){var self__ = this;
var this$__$1 = this;reagent.ratom.notify_deref_watcher_BANG_(this$__$1);
return self__.state;
});
reagent.ratom.RAtom.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (o,other){var self__ = this;
var o__$1 = this;return (o__$1 === other);
});
reagent.ratom.__GT_RAtom = (function __GT_RAtom(state,meta,validator,watches){return (new reagent.ratom.RAtom(state,meta,validator,watches));
});
/**
* Like clojure.core/atom, except that it keeps track of derefs.
* @param {...*} var_args
*/
reagent.ratom.atom = (function() {
var atom = null;
var atom__1 = (function (x){return (new reagent.ratom.RAtom(x,null,null,null));
});
var atom__2 = (function() { 
var G__9763__delegate = function (x,p__9760){var map__9762 = p__9760;var map__9762__$1 = ((cljs.core.seq_QMARK_(map__9762))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__9762):map__9762);var validator = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9762__$1,cljs.core.constant$keyword$181);var meta = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9762__$1,cljs.core.constant$keyword$175);return (new reagent.ratom.RAtom(x,meta,validator,null));
};
var G__9763 = function (x,var_args){
var p__9760 = null;if (arguments.length > 1) {
  p__9760 = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);} 
return G__9763__delegate.call(this,x,p__9760);};
G__9763.cljs$lang$maxFixedArity = 1;
G__9763.cljs$lang$applyTo = (function (arglist__9764){
var x = cljs.core.first(arglist__9764);
var p__9760 = cljs.core.rest(arglist__9764);
return G__9763__delegate(x,p__9760);
});
G__9763.cljs$core$IFn$_invoke$arity$variadic = G__9763__delegate;
return G__9763;
})()
;
atom = function(x,var_args){
var p__9760 = var_args;
switch(arguments.length){
case 1:
return atom__1.call(this,x);
default:
return atom__2.cljs$core$IFn$_invoke$arity$variadic(x, cljs.core.array_seq(arguments, 1));
}
throw(new Error('Invalid arity: ' + arguments.length));
};
atom.cljs$lang$maxFixedArity = 1;
atom.cljs$lang$applyTo = atom__2.cljs$lang$applyTo;
atom.cljs$core$IFn$_invoke$arity$1 = atom__1;
atom.cljs$core$IFn$_invoke$arity$variadic = atom__2.cljs$core$IFn$_invoke$arity$variadic;
return atom;
})()
;
reagent.ratom.IDisposable = (function (){var obj9766 = {};return obj9766;
})();
reagent.ratom.dispose_BANG_ = (function dispose_BANG_(this$){if((function (){var and__3457__auto__ = this$;if(and__3457__auto__)
{return this$.reagent$ratom$IDisposable$dispose_BANG_$arity$1;
} else
{return and__3457__auto__;
}
})())
{return this$.reagent$ratom$IDisposable$dispose_BANG_$arity$1(this$);
} else
{var x__4096__auto__ = (((this$ == null))?null:this$);return (function (){var or__3469__auto__ = (reagent.ratom.dispose_BANG_[goog.typeOf(x__4096__auto__)]);if(or__3469__auto__)
{return or__3469__auto__;
} else
{var or__3469__auto____$1 = (reagent.ratom.dispose_BANG_["_"]);if(or__3469__auto____$1)
{return or__3469__auto____$1;
} else
{throw cljs.core.missing_protocol("IDisposable.dispose!",this$);
}
}
})().call(null,this$);
}
});
reagent.ratom.IRunnable = (function (){var obj9768 = {};return obj9768;
})();
reagent.ratom.run = (function run(this$){if((function (){var and__3457__auto__ = this$;if(and__3457__auto__)
{return this$.reagent$ratom$IRunnable$run$arity$1;
} else
{return and__3457__auto__;
}
})())
{return this$.reagent$ratom$IRunnable$run$arity$1(this$);
} else
{var x__4096__auto__ = (((this$ == null))?null:this$);return (function (){var or__3469__auto__ = (reagent.ratom.run[goog.typeOf(x__4096__auto__)]);if(or__3469__auto__)
{return or__3469__auto__;
} else
{var or__3469__auto____$1 = (reagent.ratom.run["_"]);if(or__3469__auto____$1)
{return or__3469__auto____$1;
} else
{throw cljs.core.missing_protocol("IRunnable.run",this$);
}
}
})().call(null,this$);
}
});
reagent.ratom.IComputedImpl = (function (){var obj9770 = {};return obj9770;
})();
reagent.ratom._update_watching = (function _update_watching(this$,derefed){if((function (){var and__3457__auto__ = this$;if(and__3457__auto__)
{return this$.reagent$ratom$IComputedImpl$_update_watching$arity$2;
} else
{return and__3457__auto__;
}
})())
{return this$.reagent$ratom$IComputedImpl$_update_watching$arity$2(this$,derefed);
} else
{var x__4096__auto__ = (((this$ == null))?null:this$);return (function (){var or__3469__auto__ = (reagent.ratom._update_watching[goog.typeOf(x__4096__auto__)]);if(or__3469__auto__)
{return or__3469__auto__;
} else
{var or__3469__auto____$1 = (reagent.ratom._update_watching["_"]);if(or__3469__auto____$1)
{return or__3469__auto____$1;
} else
{throw cljs.core.missing_protocol("IComputedImpl.-update-watching",this$);
}
}
})().call(null,this$,derefed);
}
});
reagent.ratom._handle_change = (function _handle_change(k,sender,oldval,newval){if((function (){var and__3457__auto__ = k;if(and__3457__auto__)
{return k.reagent$ratom$IComputedImpl$_handle_change$arity$4;
} else
{return and__3457__auto__;
}
})())
{return k.reagent$ratom$IComputedImpl$_handle_change$arity$4(k,sender,oldval,newval);
} else
{var x__4096__auto__ = (((k == null))?null:k);return (function (){var or__3469__auto__ = (reagent.ratom._handle_change[goog.typeOf(x__4096__auto__)]);if(or__3469__auto__)
{return or__3469__auto__;
} else
{var or__3469__auto____$1 = (reagent.ratom._handle_change["_"]);if(or__3469__auto____$1)
{return or__3469__auto____$1;
} else
{throw cljs.core.missing_protocol("IComputedImpl.-handle-change",k);
}
}
})().call(null,k,sender,oldval,newval);
}
});
reagent.ratom.call_watches = (function call_watches(obs,watches,oldval,newval){return cljs.core.reduce_kv((function (_,key,f){(f.cljs$core$IFn$_invoke$arity$4 ? f.cljs$core$IFn$_invoke$arity$4(key,obs,oldval,newval) : f.call(null,key,obs,oldval,newval));
return null;
}),null,watches);
});

/**
* @constructor
*/
reagent.ratom.Reaction = (function (f,state,dirty_QMARK_,active_QMARK_,watching,watches,auto_run,on_set,on_dispose){
this.f = f;
this.state = state;
this.dirty_QMARK_ = dirty_QMARK_;
this.active_QMARK_ = active_QMARK_;
this.watching = watching;
this.watches = watches;
this.auto_run = auto_run;
this.on_set = on_set;
this.on_dispose = on_dispose;
this.cljs$lang$protocol_mask$partition0$ = 2153807872;
this.cljs$lang$protocol_mask$partition1$ = 114690;
})
reagent.ratom.Reaction.cljs$lang$type = true;
reagent.ratom.Reaction.cljs$lang$ctorStr = "reagent.ratom/Reaction";
reagent.ratom.Reaction.cljs$lang$ctorPrWriter = (function (this__4036__auto__,writer__4037__auto__,opt__4038__auto__){return cljs.core._write(writer__4037__auto__,"reagent.ratom/Reaction");
});
reagent.ratom.Reaction.prototype.reagent$ratom$IComputedImpl$ = true;
reagent.ratom.Reaction.prototype.reagent$ratom$IComputedImpl$_handle_change$arity$4 = (function (this$,sender,oldval,newval){var self__ = this;
var this$__$1 = this;if(cljs.core.truth_((function (){var and__3457__auto__ = self__.active_QMARK_;if(cljs.core.truth_(and__3457__auto__))
{return (cljs.core.not(self__.dirty_QMARK_)) && (!((oldval === newval)));
} else
{return and__3457__auto__;
}
})()))
{self__.dirty_QMARK_ = true;
return (function (){var or__3469__auto__ = self__.auto_run;if(cljs.core.truth_(or__3469__auto__))
{return or__3469__auto__;
} else
{return reagent.ratom.run;
}
})().call(null,this$__$1);
} else
{return null;
}
});
reagent.ratom.Reaction.prototype.reagent$ratom$IComputedImpl$_update_watching$arity$2 = (function (this$,derefed){var self__ = this;
var this$__$1 = this;var seq__9771_9783 = cljs.core.seq(derefed);var chunk__9772_9784 = null;var count__9773_9785 = 0;var i__9774_9786 = 0;while(true){
if((i__9774_9786 < count__9773_9785))
{var w_9787 = chunk__9772_9784.cljs$core$IIndexed$_nth$arity$2(null,i__9774_9786);if(cljs.core.contains_QMARK_(self__.watching,w_9787))
{} else
{cljs.core.add_watch(w_9787,this$__$1,reagent.ratom._handle_change);
}
{
var G__9788 = seq__9771_9783;
var G__9789 = chunk__9772_9784;
var G__9790 = count__9773_9785;
var G__9791 = (i__9774_9786 + 1);
seq__9771_9783 = G__9788;
chunk__9772_9784 = G__9789;
count__9773_9785 = G__9790;
i__9774_9786 = G__9791;
continue;
}
} else
{var temp__4126__auto___9792 = cljs.core.seq(seq__9771_9783);if(temp__4126__auto___9792)
{var seq__9771_9793__$1 = temp__4126__auto___9792;if(cljs.core.chunked_seq_QMARK_(seq__9771_9793__$1))
{var c__4225__auto___9794 = cljs.core.chunk_first(seq__9771_9793__$1);{
var G__9795 = cljs.core.chunk_rest(seq__9771_9793__$1);
var G__9796 = c__4225__auto___9794;
var G__9797 = cljs.core.count(c__4225__auto___9794);
var G__9798 = 0;
seq__9771_9783 = G__9795;
chunk__9772_9784 = G__9796;
count__9773_9785 = G__9797;
i__9774_9786 = G__9798;
continue;
}
} else
{var w_9799 = cljs.core.first(seq__9771_9793__$1);if(cljs.core.contains_QMARK_(self__.watching,w_9799))
{} else
{cljs.core.add_watch(w_9799,this$__$1,reagent.ratom._handle_change);
}
{
var G__9800 = cljs.core.next(seq__9771_9793__$1);
var G__9801 = null;
var G__9802 = 0;
var G__9803 = 0;
seq__9771_9783 = G__9800;
chunk__9772_9784 = G__9801;
count__9773_9785 = G__9802;
i__9774_9786 = G__9803;
continue;
}
}
} else
{}
}
break;
}
var seq__9775_9804 = cljs.core.seq(self__.watching);var chunk__9776_9805 = null;var count__9777_9806 = 0;var i__9778_9807 = 0;while(true){
if((i__9778_9807 < count__9777_9806))
{var w_9808 = chunk__9776_9805.cljs$core$IIndexed$_nth$arity$2(null,i__9778_9807);if(cljs.core.contains_QMARK_(derefed,w_9808))
{} else
{cljs.core.remove_watch(w_9808,this$__$1);
}
{
var G__9809 = seq__9775_9804;
var G__9810 = chunk__9776_9805;
var G__9811 = count__9777_9806;
var G__9812 = (i__9778_9807 + 1);
seq__9775_9804 = G__9809;
chunk__9776_9805 = G__9810;
count__9777_9806 = G__9811;
i__9778_9807 = G__9812;
continue;
}
} else
{var temp__4126__auto___9813 = cljs.core.seq(seq__9775_9804);if(temp__4126__auto___9813)
{var seq__9775_9814__$1 = temp__4126__auto___9813;if(cljs.core.chunked_seq_QMARK_(seq__9775_9814__$1))
{var c__4225__auto___9815 = cljs.core.chunk_first(seq__9775_9814__$1);{
var G__9816 = cljs.core.chunk_rest(seq__9775_9814__$1);
var G__9817 = c__4225__auto___9815;
var G__9818 = cljs.core.count(c__4225__auto___9815);
var G__9819 = 0;
seq__9775_9804 = G__9816;
chunk__9776_9805 = G__9817;
count__9777_9806 = G__9818;
i__9778_9807 = G__9819;
continue;
}
} else
{var w_9820 = cljs.core.first(seq__9775_9814__$1);if(cljs.core.contains_QMARK_(derefed,w_9820))
{} else
{cljs.core.remove_watch(w_9820,this$__$1);
}
{
var G__9821 = cljs.core.next(seq__9775_9814__$1);
var G__9822 = null;
var G__9823 = 0;
var G__9824 = 0;
seq__9775_9804 = G__9821;
chunk__9776_9805 = G__9822;
count__9777_9806 = G__9823;
i__9778_9807 = G__9824;
continue;
}
}
} else
{}
}
break;
}
return self__.watching = derefed;
});
reagent.ratom.Reaction.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this$,writer,opts){var self__ = this;
var this$__$1 = this;cljs.core._write(writer,("#<Reaction "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.hash(this$__$1))+": "));
cljs.core.pr_writer(self__.state,writer,opts);
return cljs.core._write(writer,">");
});
reagent.ratom.Reaction.prototype.cljs$core$IHash$_hash$arity$1 = (function (this$){var self__ = this;
var this$__$1 = this;return goog.getUid(this$__$1);
});
reagent.ratom.Reaction.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (o,other){var self__ = this;
var o__$1 = this;return (o__$1 === other);
});
reagent.ratom.Reaction.prototype.reagent$ratom$IDisposable$ = true;
reagent.ratom.Reaction.prototype.reagent$ratom$IDisposable$dispose_BANG_$arity$1 = (function (this$){var self__ = this;
var this$__$1 = this;var seq__9779_9825 = cljs.core.seq(self__.watching);var chunk__9780_9826 = null;var count__9781_9827 = 0;var i__9782_9828 = 0;while(true){
if((i__9782_9828 < count__9781_9827))
{var w_9829 = chunk__9780_9826.cljs$core$IIndexed$_nth$arity$2(null,i__9782_9828);cljs.core.remove_watch(w_9829,this$__$1);
{
var G__9830 = seq__9779_9825;
var G__9831 = chunk__9780_9826;
var G__9832 = count__9781_9827;
var G__9833 = (i__9782_9828 + 1);
seq__9779_9825 = G__9830;
chunk__9780_9826 = G__9831;
count__9781_9827 = G__9832;
i__9782_9828 = G__9833;
continue;
}
} else
{var temp__4126__auto___9834 = cljs.core.seq(seq__9779_9825);if(temp__4126__auto___9834)
{var seq__9779_9835__$1 = temp__4126__auto___9834;if(cljs.core.chunked_seq_QMARK_(seq__9779_9835__$1))
{var c__4225__auto___9836 = cljs.core.chunk_first(seq__9779_9835__$1);{
var G__9837 = cljs.core.chunk_rest(seq__9779_9835__$1);
var G__9838 = c__4225__auto___9836;
var G__9839 = cljs.core.count(c__4225__auto___9836);
var G__9840 = 0;
seq__9779_9825 = G__9837;
chunk__9780_9826 = G__9838;
count__9781_9827 = G__9839;
i__9782_9828 = G__9840;
continue;
}
} else
{var w_9841 = cljs.core.first(seq__9779_9835__$1);cljs.core.remove_watch(w_9841,this$__$1);
{
var G__9842 = cljs.core.next(seq__9779_9835__$1);
var G__9843 = null;
var G__9844 = 0;
var G__9845 = 0;
seq__9779_9825 = G__9842;
chunk__9780_9826 = G__9843;
count__9781_9827 = G__9844;
i__9782_9828 = G__9845;
continue;
}
}
} else
{}
}
break;
}
self__.watching = cljs.core.PersistentHashSet.EMPTY;
self__.state = null;
self__.dirty_QMARK_ = true;
if(cljs.core.truth_(self__.active_QMARK_))
{if(cljs.core.truth_(reagent.ratom.debug))
{cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(reagent.ratom._running,cljs.core.dec);
} else
{}
self__.active_QMARK_ = false;
} else
{}
if(cljs.core.truth_(self__.on_dispose))
{return (self__.on_dispose.cljs$core$IFn$_invoke$arity$0 ? self__.on_dispose.cljs$core$IFn$_invoke$arity$0() : self__.on_dispose.call(null));
} else
{return null;
}
});
reagent.ratom.Reaction.prototype.cljs$core$IReset$_reset_BANG_$arity$2 = (function (a,new_value){var self__ = this;
var a__$1 = this;var old_value = self__.state;self__.state = new_value;
cljs.core._notify_watches(a__$1,old_value,new_value);
return new_value;
});
reagent.ratom.Reaction.prototype.cljs$core$ISwap$_swap_BANG_$arity$2 = (function (a,f__$1){var self__ = this;
var a__$1 = this;return cljs.core._reset_BANG_(a__$1,(f__$1.cljs$core$IFn$_invoke$arity$1 ? f__$1.cljs$core$IFn$_invoke$arity$1(self__.state) : f__$1.call(null,self__.state)));
});
reagent.ratom.Reaction.prototype.cljs$core$ISwap$_swap_BANG_$arity$3 = (function (a,f__$1,x){var self__ = this;
var a__$1 = this;return cljs.core._reset_BANG_(a__$1,(f__$1.cljs$core$IFn$_invoke$arity$2 ? f__$1.cljs$core$IFn$_invoke$arity$2(self__.state,x) : f__$1.call(null,self__.state,x)));
});
reagent.ratom.Reaction.prototype.cljs$core$ISwap$_swap_BANG_$arity$4 = (function (a,f__$1,x,y){var self__ = this;
var a__$1 = this;return cljs.core._reset_BANG_(a__$1,(f__$1.cljs$core$IFn$_invoke$arity$3 ? f__$1.cljs$core$IFn$_invoke$arity$3(self__.state,x,y) : f__$1.call(null,self__.state,x,y)));
});
reagent.ratom.Reaction.prototype.cljs$core$ISwap$_swap_BANG_$arity$5 = (function (a,f__$1,x,y,more){var self__ = this;
var a__$1 = this;return cljs.core._reset_BANG_(a__$1,cljs.core.apply.cljs$core$IFn$_invoke$arity$5(f__$1,self__.state,x,y,more));
});
reagent.ratom.Reaction.prototype.reagent$ratom$IRunnable$ = true;
reagent.ratom.Reaction.prototype.reagent$ratom$IRunnable$run$arity$1 = (function (this$){var self__ = this;
var this$__$1 = this;var oldstate = self__.state;var res = reagent.ratom.capture_derefed(self__.f,this$__$1);var derefed = reagent.ratom.captured(this$__$1);if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(derefed,self__.watching))
{reagent.ratom._update_watching(this$__$1,derefed);
} else
{}
if(cljs.core.truth_(self__.active_QMARK_))
{} else
{if(cljs.core.truth_(reagent.ratom.debug))
{cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(reagent.ratom._running,cljs.core.inc);
} else
{}
self__.active_QMARK_ = true;
}
self__.dirty_QMARK_ = false;
self__.state = res;
reagent.ratom.call_watches(this$__$1,self__.watches,oldstate,self__.state);
return res;
});
reagent.ratom.Reaction.prototype.cljs$core$IWatchable$_notify_watches$arity$3 = (function (this$,oldval,newval){var self__ = this;
var this$__$1 = this;if(cljs.core.truth_(self__.on_set))
{(self__.on_set.cljs$core$IFn$_invoke$arity$2 ? self__.on_set.cljs$core$IFn$_invoke$arity$2(oldval,newval) : self__.on_set.call(null,oldval,newval));
} else
{}
return reagent.ratom.call_watches(this$__$1,self__.watches,oldval,newval);
});
reagent.ratom.Reaction.prototype.cljs$core$IWatchable$_add_watch$arity$3 = (function (this$,k,wf){var self__ = this;
var this$__$1 = this;return self__.watches = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.watches,k,wf);
});
reagent.ratom.Reaction.prototype.cljs$core$IWatchable$_remove_watch$arity$2 = (function (this$,k){var self__ = this;
var this$__$1 = this;self__.watches = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.watches,k);
if(cljs.core.empty_QMARK_(self__.watches))
{return reagent.ratom.dispose_BANG_(this$__$1);
} else
{return null;
}
});
reagent.ratom.Reaction.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){var self__ = this;
var this$__$1 = this;if(cljs.core.not((function (){var or__3469__auto__ = self__.auto_run;if(cljs.core.truth_(or__3469__auto__))
{return or__3469__auto__;
} else
{return reagent.ratom._STAR_ratom_context_STAR_;
}
})()))
{var x__5023__auto___9846 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [self__.auto_run,reagent.ratom._STAR_ratom_context_STAR_], null);if(!((console.log == null)))
{console.log((''+cljs.core.str.cljs$core$IFn$_invoke$arity$1(("dbg reagent.ratom:"+177+": [auto-run *ratom-context*]: "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([x__5023__auto___9846], 0)))))));
} else
{}
} else
{}
reagent.ratom.notify_deref_watcher_BANG_(this$__$1);
if(cljs.core.truth_(self__.dirty_QMARK_))
{return reagent.ratom.run(this$__$1);
} else
{return self__.state;
}
});
reagent.ratom.__GT_Reaction = (function __GT_Reaction(f,state,dirty_QMARK_,active_QMARK_,watching,watches,auto_run,on_set,on_dispose){return (new reagent.ratom.Reaction(f,state,dirty_QMARK_,active_QMARK_,watching,watches,auto_run,on_set,on_dispose));
});
/**
* @param {...*} var_args
*/
reagent.ratom.make_reaction = (function() { 
var make_reaction__delegate = function (f,p__9847){var map__9849 = p__9847;var map__9849__$1 = ((cljs.core.seq_QMARK_(map__9849))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__9849):map__9849);var derefed = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9849__$1,cljs.core.constant$keyword$220);var on_dispose = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9849__$1,cljs.core.constant$keyword$221);var on_set = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9849__$1,cljs.core.constant$keyword$222);var auto_run = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__9849__$1,cljs.core.constant$keyword$223);var runner = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(auto_run,true))?reagent.ratom.run:auto_run);var active = !((derefed == null));var dirty = !(active);var reaction = (new reagent.ratom.Reaction(f,null,dirty,active,null,cljs.core.PersistentArrayMap.EMPTY,runner,on_set,on_dispose));if((derefed == null))
{} else
{if(cljs.core.truth_(reagent.ratom.debug))
{cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(reagent.ratom._running,cljs.core.inc);
} else
{}
reaction.reagent$ratom$IComputedImpl$_update_watching$arity$2(null,derefed);
}
return reaction;
};
var make_reaction = function (f,var_args){
var p__9847 = null;if (arguments.length > 1) {
  p__9847 = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);} 
return make_reaction__delegate.call(this,f,p__9847);};
make_reaction.cljs$lang$maxFixedArity = 1;
make_reaction.cljs$lang$applyTo = (function (arglist__9850){
var f = cljs.core.first(arglist__9850);
var p__9847 = cljs.core.rest(arglist__9850);
return make_reaction__delegate(f,p__9847);
});
make_reaction.cljs$core$IFn$_invoke$arity$variadic = make_reaction__delegate;
return make_reaction;
})()
;
