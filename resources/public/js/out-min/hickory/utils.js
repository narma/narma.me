// Compiled by ClojureScript 0.0-2227
goog.provide('hickory.utils');
goog.require('cljs.core');
goog.require('goog.string');
goog.require('goog.string');
goog.require('clojure.string');
goog.require('clojure.string');
/**
* Elements that don't have a meaningful <tag></tag> form.
*/
hickory.utils.void_element = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 16, [cljs.core.constant$keyword$308,null,cljs.core.constant$keyword$175,null,cljs.core.constant$keyword$309,null,cljs.core.constant$keyword$310,null,cljs.core.constant$keyword$311,null,cljs.core.constant$keyword$312,null,cljs.core.constant$keyword$241,null,cljs.core.constant$keyword$313,null,cljs.core.constant$keyword$314,null,cljs.core.constant$keyword$315,null,cljs.core.constant$keyword$240,null,cljs.core.constant$keyword$316,null,cljs.core.constant$keyword$317,null,cljs.core.constant$keyword$318,null,cljs.core.constant$keyword$248,null,cljs.core.constant$keyword$319,null], null), null);
/**
* Elements whose content should never have html-escape codes.
*/
hickory.utils.unescapable_content = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$320,null,cljs.core.constant$keyword$219,null], null), null);
hickory.utils.html_escape = (function html_escape(s){return goog.string.htmlEscape(s);
});
hickory.utils.starts_with = (function starts_with(s,prefix){return goog.string.startsWith(s,prefix);
});
/**
* Converts its string argument into a lowercase keyword.
*/
hickory.utils.lower_case_keyword = (function lower_case_keyword(s){return cljs.core.keyword.cljs$core$IFn$_invoke$arity$1(clojure.string.lower_case(s));
});
/**
* Returns a string containing the HTML source for the doctype with given args.
* The second and third arguments can be nil or empty strings.
*/
hickory.utils.render_doctype = (function render_doctype(name,publicid,systemid){return ("<!DOCTYPE "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(name)+cljs.core.str.cljs$core$IFn$_invoke$arity$1((cljs.core.truth_(cljs.core.not_empty(publicid))?(" PUBLIC \""+cljs.core.str.cljs$core$IFn$_invoke$arity$1(publicid)+"\""):null))+cljs.core.str.cljs$core$IFn$_invoke$arity$1((cljs.core.truth_(cljs.core.not_empty(systemid))?(" \""+cljs.core.str.cljs$core$IFn$_invoke$arity$1(systemid)+"\""):null))+">");
});
