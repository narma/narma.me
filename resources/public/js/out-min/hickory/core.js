// Compiled by ClojureScript 0.0-2227
goog.provide('hickory.core');
goog.require('cljs.core');
goog.require('goog.string');
goog.require('goog.string');
goog.require('clojure.zip');
goog.require('clojure.zip');
goog.require('hickory.utils');
goog.require('hickory.utils');
hickory.core.HiccupRepresentable = (function (){var obj10959 = {};return obj10959;
})();
hickory.core.as_hiccup = (function as_hiccup(this$){if((function (){var and__3457__auto__ = this$;if(and__3457__auto__)
{return this$.hickory$core$HiccupRepresentable$as_hiccup$arity$1;
} else
{return and__3457__auto__;
}
})())
{return this$.hickory$core$HiccupRepresentable$as_hiccup$arity$1(this$);
} else
{var x__4096__auto__ = (((this$ == null))?null:this$);return (function (){var or__3469__auto__ = (hickory.core.as_hiccup[goog.typeOf(x__4096__auto__)]);if(or__3469__auto__)
{return or__3469__auto__;
} else
{var or__3469__auto____$1 = (hickory.core.as_hiccup["_"]);if(or__3469__auto____$1)
{return or__3469__auto____$1;
} else
{throw cljs.core.missing_protocol("HiccupRepresentable.as-hiccup",this$);
}
}
})().call(null,this$);
}
});
hickory.core.HickoryRepresentable = (function (){var obj10961 = {};return obj10961;
})();
hickory.core.as_hickory = (function as_hickory(this$){if((function (){var and__3457__auto__ = this$;if(and__3457__auto__)
{return this$.hickory$core$HickoryRepresentable$as_hickory$arity$1;
} else
{return and__3457__auto__;
}
})())
{return this$.hickory$core$HickoryRepresentable$as_hickory$arity$1(this$);
} else
{var x__4096__auto__ = (((this$ == null))?null:this$);return (function (){var or__3469__auto__ = (hickory.core.as_hickory[goog.typeOf(x__4096__auto__)]);if(or__3469__auto__)
{return or__3469__auto__;
} else
{var or__3469__auto____$1 = (hickory.core.as_hickory["_"]);if(or__3469__auto____$1)
{return or__3469__auto____$1;
} else
{throw cljs.core.missing_protocol("HickoryRepresentable.as-hickory",this$);
}
}
})().call(null,this$);
}
});
hickory.core.node_type = (function node_type(type){return (Node[(''+cljs.core.str.cljs$core$IFn$_invoke$arity$1(type)+"_NODE")]);
});
hickory.core.Attribute = hickory.core.node_type("ATTRIBUTE");
hickory.core.Comment = hickory.core.node_type("COMMENT");
hickory.core.Document = hickory.core.node_type("DOCUMENT");
hickory.core.DocumentType = hickory.core.node_type("DOCUMENT_TYPE");
hickory.core.Element = hickory.core.node_type("ELEMENT");
hickory.core.Text = hickory.core.node_type("TEXT");
hickory.core.extend_type_with_seqable = (function extend_type_with_seqable(t){t.prototype.cljs$core$ISeqable$ = true;
return t.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (array){var array__$1 = this;return cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(array__$1);
});
});
hickory.core.extend_type_with_seqable(NodeList);
if(typeof NamedNodeMap !== 'undefined')
{hickory.core.extend_type_with_seqable(NamedNodeMap);
} else
{}
if(typeof MozNamedAttrMap !== 'undefined')
{hickory.core.extend_type_with_seqable(MozNamedAttrMap);
} else
{}
hickory.core.format_doctype = (function format_doctype(dt){var name = (dt["name"]);var publicId = (dt["publicId"]);var systemId = (dt["systemId"]);if(!(cljs.core.empty_QMARK_(publicId)))
{return goog.string.format("<!DOCTYPE %s PUBLIC \"%s\" \"%s\">",name,publicId,systemId);
} else
{return ("<!DOCTYPE "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(name)+">");
}
});
(hickory.core.HiccupRepresentable["object"] = true);
(hickory.core.as_hiccup["object"] = (function (this$){var pred__10963 = cljs.core._EQ_;var expr__10964 = (this$["nodeType"]);if(cljs.core.truth_((pred__10963.cljs$core$IFn$_invoke$arity$2 ? pred__10963.cljs$core$IFn$_invoke$arity$2(hickory.core.Attribute,expr__10964) : pred__10963.call(null,hickory.core.Attribute,expr__10964))))
{return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [hickory.utils.lower_case_keyword((this$["name"])),(this$["value"])], null);
} else
{if(cljs.core.truth_((pred__10963.cljs$core$IFn$_invoke$arity$2 ? pred__10963.cljs$core$IFn$_invoke$arity$2(hickory.core.Comment,expr__10964) : pred__10963.call(null,hickory.core.Comment,expr__10964))))
{return ("<!--"+cljs.core.str.cljs$core$IFn$_invoke$arity$1((this$["data"]))+"-->");
} else
{if(cljs.core.truth_((pred__10963.cljs$core$IFn$_invoke$arity$2 ? pred__10963.cljs$core$IFn$_invoke$arity$2(hickory.core.Document,expr__10964) : pred__10963.call(null,hickory.core.Document,expr__10964))))
{return cljs.core.map.cljs$core$IFn$_invoke$arity$2(hickory.core.as_hiccup,(this$["childNodes"]));
} else
{if(cljs.core.truth_((pred__10963.cljs$core$IFn$_invoke$arity$2 ? pred__10963.cljs$core$IFn$_invoke$arity$2(hickory.core.DocumentType,expr__10964) : pred__10963.call(null,hickory.core.DocumentType,expr__10964))))
{return hickory.core.format_doctype(this$);
} else
{if(cljs.core.truth_((pred__10963.cljs$core$IFn$_invoke$arity$2 ? pred__10963.cljs$core$IFn$_invoke$arity$2(hickory.core.Element,expr__10964) : pred__10963.call(null,hickory.core.Element,expr__10964))))
{var tag = hickory.utils.lower_case_keyword((this$["tagName"]));return cljs.core.into(cljs.core.PersistentVector.EMPTY,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tag,cljs.core.into(cljs.core.PersistentArrayMap.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$2(hickory.core.as_hiccup,(this$["attributes"])))], null),(cljs.core.truth_((hickory.utils.unescapable_content.cljs$core$IFn$_invoke$arity$1 ? hickory.utils.unescapable_content.cljs$core$IFn$_invoke$arity$1(tag) : hickory.utils.unescapable_content.call(null,tag)))?cljs.core.map.cljs$core$IFn$_invoke$arity$2(((function (tag,pred__10963,expr__10964){
return (function (p1__10962_SHARP_){return (p1__10962_SHARP_["wholeText"]);
});})(tag,pred__10963,expr__10964))
,(this$["childNodes"])):cljs.core.map.cljs$core$IFn$_invoke$arity$2(hickory.core.as_hiccup,(this$["childNodes"])))));
} else
{if(cljs.core.truth_((pred__10963.cljs$core$IFn$_invoke$arity$2 ? pred__10963.cljs$core$IFn$_invoke$arity$2(hickory.core.Text,expr__10964) : pred__10963.call(null,hickory.core.Text,expr__10964))))
{return hickory.utils.html_escape((this$["wholeText"]));
} else
{throw (new Error(("No matching clause: "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(expr__10964))));
}
}
}
}
}
}
}));
(hickory.core.HickoryRepresentable["object"] = true);
(hickory.core.as_hickory["object"] = (function (this$){var pred__10966 = cljs.core._EQ_;var expr__10967 = (this$["nodeType"]);if(cljs.core.truth_((pred__10966.cljs$core$IFn$_invoke$arity$2 ? pred__10966.cljs$core$IFn$_invoke$arity$2(hickory.core.Attribute,expr__10967) : pred__10966.call(null,hickory.core.Attribute,expr__10967))))
{return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [hickory.utils.lower_case_keyword((this$["name"])),(this$["value"])], null);
} else
{if(cljs.core.truth_((pred__10966.cljs$core$IFn$_invoke$arity$2 ? pred__10966.cljs$core$IFn$_invoke$arity$2(hickory.core.Comment,expr__10967) : pred__10966.call(null,hickory.core.Comment,expr__10967))))
{return new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$235,cljs.core.constant$keyword$321,cljs.core.constant$keyword$238,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [(this$["data"])], null)], null);
} else
{if(cljs.core.truth_((pred__10966.cljs$core$IFn$_invoke$arity$2 ? pred__10966.cljs$core$IFn$_invoke$arity$2(hickory.core.Document,expr__10967) : pred__10966.call(null,hickory.core.Document,expr__10967))))
{return new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$235,cljs.core.constant$keyword$322,cljs.core.constant$keyword$238,cljs.core.not_empty(cljs.core.into(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$2(hickory.core.as_hickory,(this$["childNodes"]))))], null);
} else
{if(cljs.core.truth_((pred__10966.cljs$core$IFn$_invoke$arity$2 ? pred__10966.cljs$core$IFn$_invoke$arity$2(hickory.core.DocumentType,expr__10967) : pred__10966.call(null,hickory.core.DocumentType,expr__10967))))
{return new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$235,cljs.core.constant$keyword$323,cljs.core.constant$keyword$298,new cljs.core.PersistentArrayMap(null, 3, [cljs.core.constant$keyword$251,(this$["name"]),cljs.core.constant$keyword$324,(this$["publicId"]),cljs.core.constant$keyword$325,(this$["systemId"])], null)], null);
} else
{if(cljs.core.truth_((pred__10966.cljs$core$IFn$_invoke$arity$2 ? pred__10966.cljs$core$IFn$_invoke$arity$2(hickory.core.Element,expr__10967) : pred__10966.call(null,hickory.core.Element,expr__10967))))
{return new cljs.core.PersistentArrayMap(null, 4, [cljs.core.constant$keyword$235,cljs.core.constant$keyword$326,cljs.core.constant$keyword$298,cljs.core.not_empty(cljs.core.into(cljs.core.PersistentArrayMap.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$2(hickory.core.as_hickory,(this$["attributes"])))),cljs.core.constant$keyword$188,hickory.utils.lower_case_keyword((this$["tagName"])),cljs.core.constant$keyword$238,cljs.core.not_empty(cljs.core.into(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$2(hickory.core.as_hickory,(this$["childNodes"]))))], null);
} else
{if(cljs.core.truth_((pred__10966.cljs$core$IFn$_invoke$arity$2 ? pred__10966.cljs$core$IFn$_invoke$arity$2(hickory.core.Text,expr__10967) : pred__10966.call(null,hickory.core.Text,expr__10967))))
{return (this$["wholeText"]);
} else
{throw (new Error(("No matching clause: "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(expr__10967))));
}
}
}
}
}
}
}));
hickory.core.extract_doctype = (function extract_doctype(s){var temp__4126__auto__ = cljs.core.second((function (){var or__3469__auto__ = cljs.core.re_find(/<!DOCTYPE ([^>]*)>/,s);if(cljs.core.truth_(or__3469__auto__))
{return or__3469__auto__;
} else
{return cljs.core.re_find(/<!doctype ([^>]*)>/,s);
}
})());if(cljs.core.truth_(temp__4126__auto__))
{var doctype = temp__4126__auto__;return cljs.core.re_find(/([^\s]*)(\s+PUBLIC\s+[\"]?([^\"]*)[\"]?\s+[\"]?([^\"]*)[\"]?)?/,doctype);
} else
{return null;
}
});
hickory.core.remove_el = (function remove_el(el){return (el["parentNode"]).removeChild(el);
});
hickory.core.parse_dom_with_domparser = (function parse_dom_with_domparser(s){if(typeof DOMParser !== 'undefined')
{return (new DOMParser()).parseFromString(s,"text/html");
} else
{return null;
}
});
/**
* Parse an HTML document (or fragment) as a DOM using document.implementation.createHTMLDocument and document.write.
*/
hickory.core.parse_dom_with_write = (function parse_dom_with_write(s){var doc = document.implementation.createHTMLDocument("");var doctype_el = (doc["doctype"]);if(cljs.core.truth_(hickory.core.extract_doctype(s)))
{} else
{hickory.core.remove_el(doctype_el);
}
var temp__4126__auto___10969 = cljs.core.first((doc["head"]["childNodes"]));if(cljs.core.truth_(temp__4126__auto___10969))
{var title_el_10970 = temp__4126__auto___10969;if(cljs.core.empty_QMARK_((title_el_10970["text"])))
{hickory.core.remove_el(title_el_10970);
} else
{}
} else
{}
doc.write(s);
return doc;
});
/**
* Parse an entire HTML document into a DOM structure that can be
* used as input to as-hiccup or as-hickory.
*/
hickory.core.parse = (function parse(s){var or__3469__auto__ = hickory.core.parse_dom_with_domparser(s);if(cljs.core.truth_(or__3469__auto__))
{return or__3469__auto__;
} else
{return hickory.core.parse_dom_with_write(s);
}
});
/**
* Parse an HTML fragment (some group of tags that might be at home somewhere
* in the tag hierarchy under <body>) into a list of DOM elements that can
* each be passed as input to as-hiccup or as-hickory.
*/
hickory.core.parse_fragment = (function parse_fragment(s){return (hickory.core.parse(s)["body"]["childNodes"]);
});
