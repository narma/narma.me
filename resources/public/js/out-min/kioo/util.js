// Compiled by ClojureScript 0.0-2227
goog.provide('kioo.util');
goog.require('cljs.core');
goog.require('clojure.string');
goog.require('clojure.string');
kioo.util._STAR_component_STAR_ = null;
/**
* Wrapper component used to mix-in lifecycle methods
* This was pulled from quiescent
*/
kioo.util.WrapComponent = React.createClass({"componentDidMount": (function (node){var this$ = this;var temp__4126__auto__ = (function (){var or__3469__auto__ = (this$.props["onMount"]);if(cljs.core.truth_(or__3469__auto__))
{return or__3469__auto__;
} else
{return (this$.props["onRender"]);
}
})();if(cljs.core.truth_(temp__4126__auto__))
{var f = temp__4126__auto__;return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(node) : f.call(null,node));
} else
{return null;
}
}), "componentDidUpdate": (function (prev_props,prev_state,node){var this$ = this;var temp__4126__auto__ = (function (){var or__3469__auto__ = (this$.props["onUpdate"]);if(cljs.core.truth_(or__3469__auto__))
{return or__3469__auto__;
} else
{return (this$.props["onRender"]);
}
})();if(cljs.core.truth_(temp__4126__auto__))
{var f = temp__4126__auto__;var _STAR_component_STAR_10943 = kioo.util._STAR_component_STAR_;try{kioo.util._STAR_component_STAR_ = this$;
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(node) : f.call(null,node));
}finally {kioo.util._STAR_component_STAR_ = _STAR_component_STAR_10943;
}} else
{return null;
}
}), "render": (function (){var this$ = this;return (this$.props["wrappee"]);
})});
kioo.util.dont_camel_case = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["aria",null,"data",null], null), null);
kioo.util.camel_case = (function camel_case(dashed){if(typeof dashed === 'string')
{return dashed;
} else
{var name_str = cljs.core.name(dashed);var vec__10945 = clojure.string.split.cljs$core$IFn$_invoke$arity$2(name_str,/-/);var start = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__10945,0,null);var parts = cljs.core.nthnext(vec__10945,1);if(cljs.core.truth_((kioo.util.dont_camel_case.cljs$core$IFn$_invoke$arity$1 ? kioo.util.dont_camel_case.cljs$core$IFn$_invoke$arity$1(start) : kioo.util.dont_camel_case.call(null,start))))
{return name_str;
} else
{return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(cljs.core.str,start,cljs.core.map.cljs$core$IFn$_invoke$arity$2(clojure.string.capitalize,parts));
}
}
});
kioo.util.attribute_map = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (p1__10946_SHARP_,p2__10947_SHARP_){return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__10946_SHARP_,cljs.core.keyword.cljs$core$IFn$_invoke$arity$1(cljs.core.name(p2__10947_SHARP_).toLowerCase()),p2__10947_SHARP_);
}),cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentVector.fromArray([cljs.core.constant$keyword$262,cljs.core.constant$keyword$263,cljs.core.constant$keyword$264,cljs.core.constant$keyword$265,cljs.core.constant$keyword$266,cljs.core.constant$keyword$267,cljs.core.constant$keyword$268,cljs.core.constant$keyword$269,cljs.core.constant$keyword$270,cljs.core.constant$keyword$271,cljs.core.constant$keyword$272,cljs.core.constant$keyword$273,cljs.core.constant$keyword$274,cljs.core.constant$keyword$275,cljs.core.constant$keyword$276,cljs.core.constant$keyword$277,cljs.core.constant$keyword$237,cljs.core.constant$keyword$278,cljs.core.constant$keyword$279,cljs.core.constant$keyword$280,cljs.core.constant$keyword$281,cljs.core.constant$keyword$282,cljs.core.constant$keyword$283,cljs.core.constant$keyword$284,cljs.core.constant$keyword$285,cljs.core.constant$keyword$286,cljs.core.constant$keyword$287,cljs.core.constant$keyword$288,cljs.core.constant$keyword$289,cljs.core.constant$keyword$290,cljs.core.constant$keyword$291,cljs.core.constant$keyword$292,cljs.core.constant$keyword$293,cljs.core.constant$keyword$294,cljs.core.constant$keyword$295,cljs.core.constant$keyword$296,cljs.core.constant$keyword$297], true)),cljs.core.constant$keyword$218,cljs.core.constant$keyword$236);
kioo.util.transform_keys = (function transform_keys(attrs){return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (m,p__10950){var vec__10951 = p__10950;var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__10951,0,null);var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__10951,1,null);return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(m,(function (){var or__3469__auto__ = (kioo.util.attribute_map.cljs$core$IFn$_invoke$arity$1 ? kioo.util.attribute_map.cljs$core$IFn$_invoke$arity$1(k) : kioo.util.attribute_map.call(null,k));if(cljs.core.truth_(or__3469__auto__))
{return or__3469__auto__;
} else
{return k;
}
})(),v);
}),cljs.core.PersistentArrayMap.EMPTY,attrs);
});
kioo.util.convert_attrs = (function convert_attrs(attrs){var style = (cljs.core.truth_(cljs.core.constant$keyword$219.cljs$core$IFn$_invoke$arity$1(attrs))?(function (){var vals = cljs.core.re_seq(/\s*([^:;]*)[:][\s]*([^;]+)/,cljs.core.constant$keyword$219.cljs$core$IFn$_invoke$arity$1(attrs));return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (vals){
return (function (m,p__10954){var vec__10955 = p__10954;var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__10955,0,null);var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__10955,1,null);var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__10955,2,null);return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(m,k,v.trim());
});})(vals))
,cljs.core.PersistentArrayMap.EMPTY,vals);
})():null);var class_name = cljs.core.constant$keyword$218.cljs$core$IFn$_invoke$arity$1(attrs);return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(kioo.util.transform_keys(attrs),cljs.core.constant$keyword$219,style);
});
kioo.util.flatten_nodes = (function flatten_nodes(nodes){return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (p1__10957_SHARP_,p2__10956_SHARP_){if(cljs.core.seq_QMARK_(p2__10956_SHARP_))
{return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(p2__10956_SHARP_,p1__10957_SHARP_);
} else
{return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(p1__10957_SHARP_,p2__10956_SHARP_);
}
}),cljs.core.List.EMPTY,cljs.core.reverse(nodes));
});
/**
* Returns a regular expression that matches the HTML attribute `attr`
* and it's value.
*/
kioo.util.attr_pattern = (function attr_pattern(attr){return cljs.core.re_pattern(("\\s+"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.name(attr))+"\\s*=\\s*['\"][^\"']+['\"]"));
});
/**
* Strip the HTML attribute `attr` and it's value from the string `s`.
*/
kioo.util.strip_attr = (function strip_attr(s,attr){if(cljs.core.truth_(s))
{return clojure.string.replace(s,kioo.util.attr_pattern(attr),"");
} else
{return null;
}
});
