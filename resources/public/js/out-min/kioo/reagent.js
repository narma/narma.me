// Compiled by ClojureScript 0.0-2227
goog.provide('kioo.reagent');
goog.require('cljs.core');
goog.require('kioo.util');
goog.require('reagent.impl.template');
goog.require('reagent.impl.template');
goog.require('kioo.util');
goog.require('kioo.util');
goog.require('kioo.core');
goog.require('kioo.core');
kioo.reagent.make_dom = (function make_dom(node){var rnode = ((cljs.core.map_QMARK_(node))?(function (){var c = cljs.core.constant$keyword$238.cljs$core$IFn$_invoke$arity$1(node);if(cljs.core.vector_QMARK_(c))
{return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$188.cljs$core$IFn$_invoke$arity$1(node),cljs.core.constant$keyword$298.cljs$core$IFn$_invoke$arity$1(node),reagent.impl.template.as_component.cljs$core$IFn$_invoke$arity$1(c)], null);
} else
{if(cljs.core.seq_QMARK_(c))
{return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (c){
return (function (p1__11194_SHARP_,p2__11195_SHARP_){return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(p1__11194_SHARP_,reagent.impl.template.as_component.cljs$core$IFn$_invoke$arity$1(p2__11195_SHARP_));
});})(c))
,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$188.cljs$core$IFn$_invoke$arity$1(node),cljs.core.constant$keyword$298.cljs$core$IFn$_invoke$arity$1(node)], null),c);
} else
{if(cljs.core.constant$keyword$178)
{return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$188.cljs$core$IFn$_invoke$arity$1(node),cljs.core.constant$keyword$298.cljs$core$IFn$_invoke$arity$1(node),c], null);
} else
{return null;
}
}
}
})():node);return reagent.impl.template.as_component.cljs$core$IFn$_invoke$arity$1(rnode);
});
kioo.reagent.content = kioo.core.content;
kioo.reagent.append = kioo.core.append;
kioo.reagent.prepend = kioo.core.prepend;
/**
* @param {...*} var_args
*/
kioo.reagent.after = (function() { 
var after__delegate = function (body){return (function (node){return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(body,node);
});
};
var after = function (var_args){
var body = null;if (arguments.length > 0) {
  body = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return after__delegate.call(this,body);};
after.cljs$lang$maxFixedArity = 0;
after.cljs$lang$applyTo = (function (arglist__11196){
var body = cljs.core.seq(arglist__11196);
return after__delegate(body);
});
after.cljs$core$IFn$_invoke$arity$variadic = after__delegate;
return after;
})()
;
/**
* @param {...*} var_args
*/
kioo.reagent.before = (function() { 
var before__delegate = function (body){return (function (node){return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (p1__11197_SHARP_,p2__11198_SHARP_){return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(p1__11197_SHARP_,p2__11198_SHARP_);
}),cljs.core._conj(cljs.core.List.EMPTY,node),body);
});
};
var before = function (var_args){
var body = null;if (arguments.length > 0) {
  body = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return before__delegate.call(this,body);};
before.cljs$lang$maxFixedArity = 0;
before.cljs$lang$applyTo = (function (arglist__11199){
var body = cljs.core.seq(arglist__11199);
return before__delegate(body);
});
before.cljs$core$IFn$_invoke$arity$variadic = before__delegate;
return before;
})()
;
kioo.reagent.substitute = kioo.core.substitute;
kioo.reagent.set_attr = kioo.core.set_attr;
kioo.reagent.remove_attr = kioo.core.remove_attr;
kioo.reagent.do__GT_ = kioo.core.do__GT_;
kioo.reagent.set_style = kioo.core.set_style;
kioo.reagent.remove_style = kioo.core.remove_style;
kioo.reagent.set_class = kioo.core.set_class;
kioo.reagent.add_class = kioo.core.add_class;
kioo.reagent.remove_class = kioo.core.remove_class;
kioo.reagent.wrap = (function wrap(tag,attrs){return (function (node){return new cljs.core.PersistentArrayMap(null, 3, [cljs.core.constant$keyword$188,tag,cljs.core.constant$keyword$298,attrs,cljs.core.constant$keyword$238,kioo.reagent.make_dom(node)], null);
});
});
kioo.reagent.unwrap = kioo.core.unwrap;
kioo.reagent.html = kioo.core.html;
kioo.reagent.html_content = kioo.core.html_content;
kioo.reagent.listen = kioo.core.listen;
