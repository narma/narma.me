// Compiled by ClojureScript 0.0-2227
goog.provide('kioo.common');
goog.require('cljs.core');
goog.require('kioo.util');
goog.require('clojure.string');
goog.require('clojure.string');
goog.require('kioo.util');
/**
* @param {...*} var_args
*/
kioo.common.content = (function() { 
var content__delegate = function (body){return (function (node){return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(node,cljs.core.constant$keyword$238,body);
});
};
var content = function (var_args){
var body = null;if (arguments.length > 0) {
  body = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return content__delegate.call(this,body);};
content.cljs$lang$maxFixedArity = 0;
content.cljs$lang$applyTo = (function (arglist__11016){
var body = cljs.core.seq(arglist__11016);
return content__delegate(body);
});
content.cljs$core$IFn$_invoke$arity$variadic = content__delegate;
return content;
})()
;
/**
* @param {...*} var_args
*/
kioo.common.append = (function() { 
var append__delegate = function (body){return (function (node){return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(node,cljs.core.constant$keyword$238,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(cljs.core.constant$keyword$238.cljs$core$IFn$_invoke$arity$1(node),body));
});
};
var append = function (var_args){
var body = null;if (arguments.length > 0) {
  body = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return append__delegate.call(this,body);};
append.cljs$lang$maxFixedArity = 0;
append.cljs$lang$applyTo = (function (arglist__11017){
var body = cljs.core.seq(arglist__11017);
return append__delegate(body);
});
append.cljs$core$IFn$_invoke$arity$variadic = append__delegate;
return append;
})()
;
/**
* @param {...*} var_args
*/
kioo.common.prepend = (function() { 
var prepend__delegate = function (body){return (function (node){return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(node,cljs.core.constant$keyword$238,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(body,cljs.core.constant$keyword$238.cljs$core$IFn$_invoke$arity$1(node)));
});
};
var prepend = function (var_args){
var body = null;if (arguments.length > 0) {
  body = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return prepend__delegate.call(this,body);};
prepend.cljs$lang$maxFixedArity = 0;
prepend.cljs$lang$applyTo = (function (arglist__11018){
var body = cljs.core.seq(arglist__11018);
return prepend__delegate(body);
});
prepend.cljs$core$IFn$_invoke$arity$variadic = prepend__delegate;
return prepend;
})()
;
/**
* @param {...*} var_args
*/
kioo.common.substitute = (function() { 
var substitute__delegate = function (body){return (function (node){return body;
});
};
var substitute = function (var_args){
var body = null;if (arguments.length > 0) {
  body = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return substitute__delegate.call(this,body);};
substitute.cljs$lang$maxFixedArity = 0;
substitute.cljs$lang$applyTo = (function (arglist__11019){
var body = cljs.core.seq(arglist__11019);
return substitute__delegate(body);
});
substitute.cljs$core$IFn$_invoke$arity$variadic = substitute__delegate;
return substitute;
})()
;
/**
* @param {...*} var_args
*/
kioo.common.set_attr = (function() { 
var set_attr__delegate = function (body){var els = cljs.core.partition.cljs$core$IFn$_invoke$arity$2(2,body);return ((function (els){
return (function (node){return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(node,cljs.core.constant$keyword$298,cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (els){
return (function (n,p__11022){var vec__11023 = p__11022;var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11023,0,null);var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11023,1,null);return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(n,k,v);
});})(els))
,cljs.core.constant$keyword$298.cljs$core$IFn$_invoke$arity$1(node),els));
});
;})(els))
};
var set_attr = function (var_args){
var body = null;if (arguments.length > 0) {
  body = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return set_attr__delegate.call(this,body);};
set_attr.cljs$lang$maxFixedArity = 0;
set_attr.cljs$lang$applyTo = (function (arglist__11024){
var body = cljs.core.seq(arglist__11024);
return set_attr__delegate(body);
});
set_attr.cljs$core$IFn$_invoke$arity$variadic = set_attr__delegate;
return set_attr;
})()
;
/**
* @param {...*} var_args
*/
kioo.common.remove_attr = (function() { 
var remove_attr__delegate = function (body){return (function (node){return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(node,cljs.core.constant$keyword$298,cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (n,k){return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(n,k);
}),cljs.core.constant$keyword$298.cljs$core$IFn$_invoke$arity$1(node),body));
});
};
var remove_attr = function (var_args){
var body = null;if (arguments.length > 0) {
  body = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return remove_attr__delegate.call(this,body);};
remove_attr.cljs$lang$maxFixedArity = 0;
remove_attr.cljs$lang$applyTo = (function (arglist__11025){
var body = cljs.core.seq(arglist__11025);
return remove_attr__delegate(body);
});
remove_attr.cljs$core$IFn$_invoke$arity$variadic = remove_attr__delegate;
return remove_attr;
})()
;
/**
* @param {...*} var_args
*/
kioo.common.do__GT_ = (function() { 
var do__GT___delegate = function (body){return (function (node){return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (p1__11027_SHARP_,p2__11026_SHARP_){return (p2__11026_SHARP_.cljs$core$IFn$_invoke$arity$1 ? p2__11026_SHARP_.cljs$core$IFn$_invoke$arity$1(p1__11027_SHARP_) : p2__11026_SHARP_.call(null,p1__11027_SHARP_));
}),node,body);
});
};
var do__GT_ = function (var_args){
var body = null;if (arguments.length > 0) {
  body = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return do__GT___delegate.call(this,body);};
do__GT_.cljs$lang$maxFixedArity = 0;
do__GT_.cljs$lang$applyTo = (function (arglist__11028){
var body = cljs.core.seq(arglist__11028);
return do__GT___delegate(body);
});
do__GT_.cljs$core$IFn$_invoke$arity$variadic = do__GT___delegate;
return do__GT_;
})()
;
/**
* @param {...*} var_args
*/
kioo.common.set_style = (function() { 
var set_style__delegate = function (body){var els = cljs.core.partition.cljs$core$IFn$_invoke$arity$2(2,body);var mp = cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(((function (els){
return (function (m,p__11032){var vec__11033 = p__11032;var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11033,0,null);var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11033,1,null);return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(m,k,v);
});})(els))
,cljs.core.PersistentArrayMap.EMPTY,els);return ((function (els,mp){
return (function (node){return cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(node,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$298,cljs.core.constant$keyword$219], null),((function (els,mp){
return (function (p1__11029_SHARP_){return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([p1__11029_SHARP_,mp], 0));
});})(els,mp))
);
});
;})(els,mp))
};
var set_style = function (var_args){
var body = null;if (arguments.length > 0) {
  body = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return set_style__delegate.call(this,body);};
set_style.cljs$lang$maxFixedArity = 0;
set_style.cljs$lang$applyTo = (function (arglist__11034){
var body = cljs.core.seq(arglist__11034);
return set_style__delegate(body);
});
set_style.cljs$core$IFn$_invoke$arity$variadic = set_style__delegate;
return set_style;
})()
;
/**
* @param {...*} var_args
*/
kioo.common.remove_style = (function() { 
var remove_style__delegate = function (body){return (function (node){var style = cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (p1__11035_SHARP_,p2__11036_SHARP_){return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(p1__11035_SHARP_,cljs.core.name(p2__11036_SHARP_),cljs.core.array_seq([p2__11036_SHARP_], 0));
}),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(node,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$298,cljs.core.constant$keyword$219], null)),body);return cljs.core.assoc_in(node,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$298,cljs.core.constant$keyword$219], null),style);
});
};
var remove_style = function (var_args){
var body = null;if (arguments.length > 0) {
  body = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return remove_style__delegate.call(this,body);};
remove_style.cljs$lang$maxFixedArity = 0;
remove_style.cljs$lang$applyTo = (function (arglist__11037){
var body = cljs.core.seq(arglist__11037);
return remove_style__delegate(body);
});
remove_style.cljs$core$IFn$_invoke$arity$variadic = remove_style__delegate;
return remove_style;
})()
;
kioo.common.get_class_regex = (function get_class_regex(cls){return (new RegExp(("(\\s|^)"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(cls)+"(\\s|$)")));
});
kioo.common.has_class_QMARK_ = (function has_class_QMARK_(cur_cls,cls){return cljs.core.re_find(kioo.common.get_class_regex(cls),cur_cls);
});
/**
* @param {...*} var_args
*/
kioo.common.set_class = (function() { 
var set_class__delegate = function (values){return (function (node){var new_class = cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (p1__11038_SHARP_,p2__11039_SHARP_){if(cljs.core.truth_(kioo.common.has_class_QMARK_(p1__11038_SHARP_,p2__11039_SHARP_)))
{return p1__11038_SHARP_;
} else
{return (''+cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__11038_SHARP_)+" "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(p2__11039_SHARP_));
}
}),"",values);return cljs.core.assoc_in(node,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$298,cljs.core.constant$keyword$236], null),new_class);
});
};
var set_class = function (var_args){
var values = null;if (arguments.length > 0) {
  values = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return set_class__delegate.call(this,values);};
set_class.cljs$lang$maxFixedArity = 0;
set_class.cljs$lang$applyTo = (function (arglist__11040){
var values = cljs.core.seq(arglist__11040);
return set_class__delegate(values);
});
set_class.cljs$core$IFn$_invoke$arity$variadic = set_class__delegate;
return set_class;
})()
;
/**
* @param {...*} var_args
*/
kioo.common.add_class = (function() { 
var add_class__delegate = function (values){return (function (node){var new_class = cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (p1__11041_SHARP_,p2__11042_SHARP_){if(cljs.core.truth_(kioo.common.has_class_QMARK_(p1__11041_SHARP_,p2__11042_SHARP_)))
{return p1__11041_SHARP_;
} else
{return (''+cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__11041_SHARP_)+" "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(p2__11042_SHARP_));
}
}),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(node,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$298,cljs.core.constant$keyword$236], null)),values);return cljs.core.assoc_in(node,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$298,cljs.core.constant$keyword$236], null),new_class);
});
};
var add_class = function (var_args){
var values = null;if (arguments.length > 0) {
  values = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return add_class__delegate.call(this,values);};
add_class.cljs$lang$maxFixedArity = 0;
add_class.cljs$lang$applyTo = (function (arglist__11043){
var values = cljs.core.seq(arglist__11043);
return add_class__delegate(values);
});
add_class.cljs$core$IFn$_invoke$arity$variadic = add_class__delegate;
return add_class;
})()
;
/**
* @param {...*} var_args
*/
kioo.common.remove_class = (function() { 
var remove_class__delegate = function (values){return (function (node){var new_class = cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (p1__11044_SHARP_,p2__11045_SHARP_){if(cljs.core.truth_(kioo.common.has_class_QMARK_(p1__11044_SHARP_,p2__11045_SHARP_)))
{return clojure.string.replace(p1__11044_SHARP_,kioo.common.get_class_regex(p2__11045_SHARP_)," ");
} else
{return p1__11044_SHARP_;
}
}),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(node,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$298,cljs.core.constant$keyword$236], null)),values);return cljs.core.assoc_in(node,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$298,cljs.core.constant$keyword$236], null),new_class);
});
};
var remove_class = function (var_args){
var values = null;if (arguments.length > 0) {
  values = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return remove_class__delegate.call(this,values);};
remove_class.cljs$lang$maxFixedArity = 0;
remove_class.cljs$lang$applyTo = (function (arglist__11046){
var values = cljs.core.seq(arglist__11046);
return remove_class__delegate(values);
});
remove_class.cljs$core$IFn$_invoke$arity$variadic = remove_class__delegate;
return remove_class;
})()
;
kioo.common.unwrap = (function unwrap(node){return cljs.core.constant$keyword$238.cljs$core$IFn$_invoke$arity$1(node);
});
