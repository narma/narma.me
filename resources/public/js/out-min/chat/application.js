// Compiled by ClojureScript 0.0-2227
goog.provide('chat.application');
goog.require('cljs.core');
goog.require('chat.render');
goog.require('chat.render');
goog.require('chat.service');
goog.require('chat.service');
goog.require('goog.events');
goog.require('goog.events');
chat.application.main = (function main(){cljs.core.enable_console_print_BANG_();
chat.service.start();
return chat.render.main(true);
});
goog.exportSymbol('chat.application.main', chat.application.main);
