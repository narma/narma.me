// Compiled by ClojureScript 0.0-2227
goog.provide('chat.history');
goog.require('cljs.core');
chat.history.back_BANG_ = (function back_BANG_(){return history.back();
});
chat.history.forward_BANG_ = (function forward_BANG_(){return history.forward();
});
chat.history.go_BANG_ = (function go_BANG_(idx){return history.go(idx);
});
chat.history.replace_state_BANG_ = (function() {
var replace_state_BANG_ = null;
var replace_state_BANG___1 = (function (state){return replace_state_BANG_.cljs$core$IFn$_invoke$arity$2(state,document.title);
});
var replace_state_BANG___2 = (function (state,title){return history.replaceState(state,title);
});
var replace_state_BANG___3 = (function (state,title,path){return history.replaceState(state,title,path);
});
replace_state_BANG_ = function(state,title,path){
switch(arguments.length){
case 1:
return replace_state_BANG___1.call(this,state);
case 2:
return replace_state_BANG___2.call(this,state,title);
case 3:
return replace_state_BANG___3.call(this,state,title,path);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
replace_state_BANG_.cljs$core$IFn$_invoke$arity$1 = replace_state_BANG___1;
replace_state_BANG_.cljs$core$IFn$_invoke$arity$2 = replace_state_BANG___2;
replace_state_BANG_.cljs$core$IFn$_invoke$arity$3 = replace_state_BANG___3;
return replace_state_BANG_;
})()
;
chat.history.push_state_BANG_ = (function() {
var push_state_BANG_ = null;
var push_state_BANG___2 = (function (state,title){return history.pushState(state,title);
});
var push_state_BANG___3 = (function (state,title,path){return history.pushState(state,title,path);
});
push_state_BANG_ = function(state,title,path){
switch(arguments.length){
case 2:
return push_state_BANG___2.call(this,state,title);
case 3:
return push_state_BANG___3.call(this,state,title,path);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
push_state_BANG_.cljs$core$IFn$_invoke$arity$2 = push_state_BANG___2;
push_state_BANG_.cljs$core$IFn$_invoke$arity$3 = push_state_BANG___3;
return push_state_BANG_;
})()
;
/**
* Returns current JS value of history.state
*/
chat.history.current_state = (function current_state(){return history.state;
});
chat.history.state = (function (){var clj_state = (function (){return cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$variadic(history.state,cljs.core.array_seq([cljs.core.constant$keyword$184,true], 0));
});if(typeof chat.history.t9477 !== 'undefined')
{} else
{
/**
* @constructor
*/
chat.history.t9477 = (function (clj_state,meta9478){
this.clj_state = clj_state;
this.meta9478 = meta9478;
this.cljs$lang$protocol_mask$partition1$ = 98304;
this.cljs$lang$protocol_mask$partition0$ = 425984;
})
chat.history.t9477.cljs$lang$type = true;
chat.history.t9477.cljs$lang$ctorStr = "chat.history/t9477";
chat.history.t9477.cljs$lang$ctorPrWriter = ((function (clj_state){
return (function (this__4036__auto__,writer__4037__auto__,opt__4038__auto__){return cljs.core._write(writer__4037__auto__,"chat.history/t9477");
});})(clj_state))
;
chat.history.t9477.prototype.cljs$core$ISwap$_swap_BANG_$arity$2 = ((function (clj_state){
return (function (s,f){var self__ = this;
var s__$1 = this;return cljs.core._reset_BANG_(s__$1,(f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1((self__.clj_state.cljs$core$IFn$_invoke$arity$0 ? self__.clj_state.cljs$core$IFn$_invoke$arity$0() : self__.clj_state.call(null))) : f.call(null,(self__.clj_state.cljs$core$IFn$_invoke$arity$0 ? self__.clj_state.cljs$core$IFn$_invoke$arity$0() : self__.clj_state.call(null)))));
});})(clj_state))
;
chat.history.t9477.prototype.cljs$core$ISwap$_swap_BANG_$arity$3 = ((function (clj_state){
return (function (s,f,x){var self__ = this;
var s__$1 = this;return cljs.core._reset_BANG_(s__$1,(f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2((self__.clj_state.cljs$core$IFn$_invoke$arity$0 ? self__.clj_state.cljs$core$IFn$_invoke$arity$0() : self__.clj_state.call(null)),x) : f.call(null,(self__.clj_state.cljs$core$IFn$_invoke$arity$0 ? self__.clj_state.cljs$core$IFn$_invoke$arity$0() : self__.clj_state.call(null)),x)));
});})(clj_state))
;
chat.history.t9477.prototype.cljs$core$ISwap$_swap_BANG_$arity$4 = ((function (clj_state){
return (function (s,f,x,y){var self__ = this;
var s__$1 = this;return cljs.core._reset_BANG_(s__$1,(f.cljs$core$IFn$_invoke$arity$3 ? f.cljs$core$IFn$_invoke$arity$3((self__.clj_state.cljs$core$IFn$_invoke$arity$0 ? self__.clj_state.cljs$core$IFn$_invoke$arity$0() : self__.clj_state.call(null)),x,y) : f.call(null,(self__.clj_state.cljs$core$IFn$_invoke$arity$0 ? self__.clj_state.cljs$core$IFn$_invoke$arity$0() : self__.clj_state.call(null)),x,y)));
});})(clj_state))
;
chat.history.t9477.prototype.cljs$core$ISwap$_swap_BANG_$arity$5 = ((function (clj_state){
return (function (s,f,x,y,more){var self__ = this;
var s__$1 = this;return cljs.core._reset_BANG_(s__$1,cljs.core.apply.cljs$core$IFn$_invoke$arity$5(f,(self__.clj_state.cljs$core$IFn$_invoke$arity$0 ? self__.clj_state.cljs$core$IFn$_invoke$arity$0() : self__.clj_state.call(null)),x,y,more));
});})(clj_state))
;
chat.history.t9477.prototype.cljs$core$IReset$_reset_BANG_$arity$2 = ((function (clj_state){
return (function (_,v){var self__ = this;
var ___$1 = this;return chat.history.replace_state_BANG_.cljs$core$IFn$_invoke$arity$1(cljs.core.clj__GT_js(v));
});})(clj_state))
;
chat.history.t9477.prototype.cljs$core$IDeref$_deref$arity$1 = ((function (clj_state){
return (function (_){var self__ = this;
var ___$1 = this;return (self__.clj_state.cljs$core$IFn$_invoke$arity$0 ? self__.clj_state.cljs$core$IFn$_invoke$arity$0() : self__.clj_state.call(null));
});})(clj_state))
;
chat.history.t9477.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (clj_state){
return (function (_9479){var self__ = this;
var _9479__$1 = this;return self__.meta9478;
});})(clj_state))
;
chat.history.t9477.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (clj_state){
return (function (_9479,meta9478__$1){var self__ = this;
var _9479__$1 = this;return (new chat.history.t9477(self__.clj_state,meta9478__$1));
});})(clj_state))
;
chat.history.__GT_t9477 = ((function (clj_state){
return (function __GT_t9477(clj_state__$1,meta9478){return (new chat.history.t9477(clj_state__$1,meta9478));
});})(clj_state))
;
}
return (new chat.history.t9477(clj_state,null));
})();
