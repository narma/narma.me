// Compiled by ClojureScript 0.0-2227
goog.provide('chat.service');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('ajax.core');
goog.require('reagent.core');
goog.require('reagent.core');
goog.require('ajax.core');
chat.service.messages = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentVector.EMPTY);
chat.service.channels = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentVector.EMPTY);
chat.service.error_handler = (function error_handler(p__12572){var map__12574 = p__12572;var map__12574__$1 = ((cljs.core.seq_QMARK_(map__12574))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__12574):map__12574);var status_text = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__12574__$1,cljs.core.constant$keyword$201);var status = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__12574__$1,cljs.core.constant$keyword$199);return console.log(("Something bad happened: "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(status)+" "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(status_text)));
});
chat.service.msg_send = (function msg_send(msg){return ajax.core.POST.cljs$core$IFn$_invoke$arity$variadic("/messages/create",cljs.core.array_seq([new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$208,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.constant$keyword$234,msg], null),cljs.core.constant$keyword$216,chat.service.error_handler], null)], 0));
});
chat.service.load_history = (function load_history(){return ajax.core.GET.cljs$core$IFn$_invoke$arity$variadic("/messages/list",cljs.core.array_seq([new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$216,chat.service.error_handler,cljs.core.constant$keyword$210,(function (p1__12575_SHARP_){var seq__12580 = cljs.core.seq(p1__12575_SHARP_);var chunk__12581 = null;var count__12582 = 0;var i__12583 = 0;while(true){
if((i__12583 < count__12582))
{var msg = chunk__12581.cljs$core$IIndexed$_nth$arity$2(null,i__12583);cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq(["import ",msg], 0));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(chat.service.messages,cljs.core.conj,msg);
{
var G__12584 = seq__12580;
var G__12585 = chunk__12581;
var G__12586 = count__12582;
var G__12587 = (i__12583 + 1);
seq__12580 = G__12584;
chunk__12581 = G__12585;
count__12582 = G__12586;
i__12583 = G__12587;
continue;
}
} else
{var temp__4126__auto__ = cljs.core.seq(seq__12580);if(temp__4126__auto__)
{var seq__12580__$1 = temp__4126__auto__;if(cljs.core.chunked_seq_QMARK_(seq__12580__$1))
{var c__4225__auto__ = cljs.core.chunk_first(seq__12580__$1);{
var G__12588 = cljs.core.chunk_rest(seq__12580__$1);
var G__12589 = c__4225__auto__;
var G__12590 = cljs.core.count(c__4225__auto__);
var G__12591 = 0;
seq__12580 = G__12588;
chunk__12581 = G__12589;
count__12582 = G__12590;
i__12583 = G__12591;
continue;
}
} else
{var msg = cljs.core.first(seq__12580__$1);cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq(["import ",msg], 0));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(chat.service.messages,cljs.core.conj,msg);
{
var G__12592 = cljs.core.next(seq__12580__$1);
var G__12593 = null;
var G__12594 = 0;
var G__12595 = 0;
seq__12580 = G__12592;
chunk__12581 = G__12593;
count__12582 = G__12594;
i__12583 = G__12595;
continue;
}
}
} else
{return null;
}
}
break;
}
})], null)], 0));
});
chat.service.pooling = (function pooling(){return ajax.core.GET.cljs$core$IFn$_invoke$arity$variadic("/messages/poll",cljs.core.array_seq([new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$216,chat.service.error_handler,cljs.core.constant$keyword$210,(function (event){var G__12597_12598 = cljs.core.constant$keyword$235.cljs$core$IFn$_invoke$arity$1(event);var caseval__12599;
switch (G__12597_12598){
case "message":
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(chat.service.messages,cljs.core.conj,event)
break;
default:
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq(["unknown msg",event], 0))
}
return pooling();
})], null)], 0));
});
chat.service.start = (function start(){return chat.service.pooling();
});
