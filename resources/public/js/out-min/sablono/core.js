// Compiled by ClojureScript 0.0-2227
goog.provide('sablono.core');
goog.require('cljs.core');
goog.require('clojure.walk');
goog.require('clojure.string');
goog.require('sablono.util');
goog.require('goog.dom');
goog.require('goog.dom');
goog.require('sablono.interpreter');
goog.require('sablono.interpreter');
goog.require('sablono.util');
goog.require('clojure.walk');
goog.require('clojure.string');
/**
* Add an optional attribute argument to a function that returns a element vector.
*/
sablono.core.wrap_attrs = (function wrap_attrs(func){return (function() { 
var G__11049__delegate = function (args){if(cljs.core.map_QMARK_(cljs.core.first(args)))
{var vec__11048 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(func,cljs.core.rest(args));var tag = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11048,0,null);var body = cljs.core.nthnext(vec__11048,1);if(cljs.core.map_QMARK_(cljs.core.first(body)))
{return cljs.core.apply.cljs$core$IFn$_invoke$arity$4(cljs.core.vector,tag,cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([cljs.core.first(body),cljs.core.first(args)], 0)),cljs.core.rest(body));
} else
{return cljs.core.apply.cljs$core$IFn$_invoke$arity$4(cljs.core.vector,tag,cljs.core.first(args),body);
}
} else
{return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(func,args);
}
};
var G__11049 = function (var_args){
var args = null;if (arguments.length > 0) {
  args = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return G__11049__delegate.call(this,args);};
G__11049.cljs$lang$maxFixedArity = 0;
G__11049.cljs$lang$applyTo = (function (arglist__11050){
var args = cljs.core.seq(arglist__11050);
return G__11049__delegate(args);
});
G__11049.cljs$core$IFn$_invoke$arity$variadic = G__11049__delegate;
return G__11049;
})()
;
});
sablono.core.update_arglists = (function update_arglists(arglists){var iter__4194__auto__ = (function iter__11055(s__11056){return (new cljs.core.LazySeq(null,(function (){var s__11056__$1 = s__11056;while(true){
var temp__4126__auto__ = cljs.core.seq(s__11056__$1);if(temp__4126__auto__)
{var s__11056__$2 = temp__4126__auto__;if(cljs.core.chunked_seq_QMARK_(s__11056__$2))
{var c__4192__auto__ = cljs.core.chunk_first(s__11056__$2);var size__4193__auto__ = cljs.core.count(c__4192__auto__);var b__11058 = cljs.core.chunk_buffer(size__4193__auto__);if((function (){var i__11057 = 0;while(true){
if((i__11057 < size__4193__auto__))
{var args = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4192__auto__,i__11057);cljs.core.chunk_append(b__11058,cljs.core.vec(cljs.core.cons(new cljs.core.Symbol(null,"attr-map?","attr-map?",-1682549128,null),args)));
{
var G__11059 = (i__11057 + 1);
i__11057 = G__11059;
continue;
}
} else
{return true;
}
break;
}
})())
{return cljs.core.chunk_cons(cljs.core.chunk(b__11058),iter__11055(cljs.core.chunk_rest(s__11056__$2)));
} else
{return cljs.core.chunk_cons(cljs.core.chunk(b__11058),null);
}
} else
{var args = cljs.core.first(s__11056__$2);return cljs.core.cons(cljs.core.vec(cljs.core.cons(new cljs.core.Symbol(null,"attr-map?","attr-map?",-1682549128,null),args)),iter__11055(cljs.core.rest(s__11056__$2)));
}
} else
{return null;
}
break;
}
}),null,null));
});return iter__4194__auto__(arglists);
});
/**
* Render the React `component` as an HTML string.
*/
sablono.core.render = (function render(component){var html = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);React.renderComponentToString(component,((function (html){
return (function (p1__11060_SHARP_){return cljs.core.reset_BANG_(html,p1__11060_SHARP_);
});})(html))
);
return cljs.core.deref(html);
});
/**
* Include a list of external stylesheet files.
* @param {...*} var_args
*/
sablono.core.include_css = (function() { 
var include_css__delegate = function (styles){var iter__4194__auto__ = (function iter__11065(s__11066){return (new cljs.core.LazySeq(null,(function (){var s__11066__$1 = s__11066;while(true){
var temp__4126__auto__ = cljs.core.seq(s__11066__$1);if(temp__4126__auto__)
{var s__11066__$2 = temp__4126__auto__;if(cljs.core.chunked_seq_QMARK_(s__11066__$2))
{var c__4192__auto__ = cljs.core.chunk_first(s__11066__$2);var size__4193__auto__ = cljs.core.count(c__4192__auto__);var b__11068 = cljs.core.chunk_buffer(size__4193__auto__);if((function (){var i__11067 = 0;while(true){
if((i__11067 < size__4193__auto__))
{var style = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4192__auto__,i__11067);cljs.core.chunk_append(b__11068,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$241,new cljs.core.PersistentArrayMap(null, 3, [cljs.core.constant$keyword$235,"text/css",cljs.core.constant$keyword$242,sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([style], 0)),cljs.core.constant$keyword$243,"stylesheet"], null)], null));
{
var G__11069 = (i__11067 + 1);
i__11067 = G__11069;
continue;
}
} else
{return true;
}
break;
}
})())
{return cljs.core.chunk_cons(cljs.core.chunk(b__11068),iter__11065(cljs.core.chunk_rest(s__11066__$2)));
} else
{return cljs.core.chunk_cons(cljs.core.chunk(b__11068),null);
}
} else
{var style = cljs.core.first(s__11066__$2);return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$241,new cljs.core.PersistentArrayMap(null, 3, [cljs.core.constant$keyword$235,"text/css",cljs.core.constant$keyword$242,sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([style], 0)),cljs.core.constant$keyword$243,"stylesheet"], null)], null),iter__11065(cljs.core.rest(s__11066__$2)));
}
} else
{return null;
}
break;
}
}),null,null));
});return iter__4194__auto__(styles);
};
var include_css = function (var_args){
var styles = null;if (arguments.length > 0) {
  styles = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return include_css__delegate.call(this,styles);};
include_css.cljs$lang$maxFixedArity = 0;
include_css.cljs$lang$applyTo = (function (arglist__11070){
var styles = cljs.core.seq(arglist__11070);
return include_css__delegate(styles);
});
include_css.cljs$core$IFn$_invoke$arity$variadic = include_css__delegate;
return include_css;
})()
;
/**
* Include the JavaScript library at `src`.
*/
sablono.core.include_js = (function include_js(src){return goog.dom.appendChild(goog.dom.getDocument().body,goog.dom.createDom("script",{"src": src}));
});
/**
* Include Facebook's React JavaScript library.
*/
sablono.core.include_react = (function include_react(){return sablono.core.include_js("http://fb.me/react-0.8.0.js");
});
/**
* Wraps some content in a HTML hyperlink with the supplied URL.
* @param {...*} var_args
*/
sablono.core.link_to11071 = (function() { 
var link_to11071__delegate = function (url,content){return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$244,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.constant$keyword$242,sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([url], 0))], null),content], null);
};
var link_to11071 = function (url,var_args){
var content = null;if (arguments.length > 1) {
  content = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);} 
return link_to11071__delegate.call(this,url,content);};
link_to11071.cljs$lang$maxFixedArity = 1;
link_to11071.cljs$lang$applyTo = (function (arglist__11072){
var url = cljs.core.first(arglist__11072);
var content = cljs.core.rest(arglist__11072);
return link_to11071__delegate(url,content);
});
link_to11071.cljs$core$IFn$_invoke$arity$variadic = link_to11071__delegate;
return link_to11071;
})()
;
sablono.core.link_to = sablono.core.wrap_attrs(sablono.core.link_to11071);
/**
* Wraps some content in a HTML hyperlink with the supplied e-mail
* address. If no content provided use the e-mail address as content.
* @param {...*} var_args
*/
sablono.core.mail_to11073 = (function() { 
var mail_to11073__delegate = function (e_mail,p__11074){var vec__11076 = p__11074;var content = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11076,0,null);return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$244,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.constant$keyword$242,("mailto:"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_mail))], null),(function (){var or__3469__auto__ = content;if(cljs.core.truth_(or__3469__auto__))
{return or__3469__auto__;
} else
{return e_mail;
}
})()], null);
};
var mail_to11073 = function (e_mail,var_args){
var p__11074 = null;if (arguments.length > 1) {
  p__11074 = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);} 
return mail_to11073__delegate.call(this,e_mail,p__11074);};
mail_to11073.cljs$lang$maxFixedArity = 1;
mail_to11073.cljs$lang$applyTo = (function (arglist__11077){
var e_mail = cljs.core.first(arglist__11077);
var p__11074 = cljs.core.rest(arglist__11077);
return mail_to11073__delegate(e_mail,p__11074);
});
mail_to11073.cljs$core$IFn$_invoke$arity$variadic = mail_to11073__delegate;
return mail_to11073;
})()
;
sablono.core.mail_to = sablono.core.wrap_attrs(sablono.core.mail_to11073);
/**
* Wrap a collection in an unordered list.
*/
sablono.core.unordered_list11078 = (function unordered_list11078(coll){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$245,(function (){var iter__4194__auto__ = (function iter__11083(s__11084){return (new cljs.core.LazySeq(null,(function (){var s__11084__$1 = s__11084;while(true){
var temp__4126__auto__ = cljs.core.seq(s__11084__$1);if(temp__4126__auto__)
{var s__11084__$2 = temp__4126__auto__;if(cljs.core.chunked_seq_QMARK_(s__11084__$2))
{var c__4192__auto__ = cljs.core.chunk_first(s__11084__$2);var size__4193__auto__ = cljs.core.count(c__4192__auto__);var b__11086 = cljs.core.chunk_buffer(size__4193__auto__);if((function (){var i__11085 = 0;while(true){
if((i__11085 < size__4193__auto__))
{var x = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4192__auto__,i__11085);cljs.core.chunk_append(b__11086,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$246,x], null));
{
var G__11087 = (i__11085 + 1);
i__11085 = G__11087;
continue;
}
} else
{return true;
}
break;
}
})())
{return cljs.core.chunk_cons(cljs.core.chunk(b__11086),iter__11083(cljs.core.chunk_rest(s__11084__$2)));
} else
{return cljs.core.chunk_cons(cljs.core.chunk(b__11086),null);
}
} else
{var x = cljs.core.first(s__11084__$2);return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$246,x], null),iter__11083(cljs.core.rest(s__11084__$2)));
}
} else
{return null;
}
break;
}
}),null,null));
});return iter__4194__auto__(coll);
})()], null);
});
sablono.core.unordered_list = sablono.core.wrap_attrs(sablono.core.unordered_list11078);
/**
* Wrap a collection in an ordered list.
*/
sablono.core.ordered_list11088 = (function ordered_list11088(coll){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$247,(function (){var iter__4194__auto__ = (function iter__11093(s__11094){return (new cljs.core.LazySeq(null,(function (){var s__11094__$1 = s__11094;while(true){
var temp__4126__auto__ = cljs.core.seq(s__11094__$1);if(temp__4126__auto__)
{var s__11094__$2 = temp__4126__auto__;if(cljs.core.chunked_seq_QMARK_(s__11094__$2))
{var c__4192__auto__ = cljs.core.chunk_first(s__11094__$2);var size__4193__auto__ = cljs.core.count(c__4192__auto__);var b__11096 = cljs.core.chunk_buffer(size__4193__auto__);if((function (){var i__11095 = 0;while(true){
if((i__11095 < size__4193__auto__))
{var x = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4192__auto__,i__11095);cljs.core.chunk_append(b__11096,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$246,x], null));
{
var G__11097 = (i__11095 + 1);
i__11095 = G__11097;
continue;
}
} else
{return true;
}
break;
}
})())
{return cljs.core.chunk_cons(cljs.core.chunk(b__11096),iter__11093(cljs.core.chunk_rest(s__11094__$2)));
} else
{return cljs.core.chunk_cons(cljs.core.chunk(b__11096),null);
}
} else
{var x = cljs.core.first(s__11094__$2);return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$246,x], null),iter__11093(cljs.core.rest(s__11094__$2)));
}
} else
{return null;
}
break;
}
}),null,null));
});return iter__4194__auto__(coll);
})()], null);
});
sablono.core.ordered_list = sablono.core.wrap_attrs(sablono.core.ordered_list11088);
/**
* Create an image element.
*/
sablono.core.image11098 = (function() {
var image11098 = null;
var image11098__1 = (function (src){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$248,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.constant$keyword$249,sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([src], 0))], null)], null);
});
var image11098__2 = (function (src,alt){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$248,new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$249,sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([src], 0)),cljs.core.constant$keyword$250,alt], null)], null);
});
image11098 = function(src,alt){
switch(arguments.length){
case 1:
return image11098__1.call(this,src);
case 2:
return image11098__2.call(this,src,alt);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
image11098.cljs$core$IFn$_invoke$arity$1 = image11098__1;
image11098.cljs$core$IFn$_invoke$arity$2 = image11098__2;
return image11098;
})()
;
sablono.core.image = sablono.core.wrap_attrs(sablono.core.image11098);
sablono.core._STAR_group_STAR_ = cljs.core.PersistentVector.EMPTY;
/**
* Create a field name from the supplied argument the current field group.
*/
sablono.core.make_name = (function make_name(name){return cljs.core.reduce.cljs$core$IFn$_invoke$arity$2((function (p1__11099_SHARP_,p2__11100_SHARP_){return (''+cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__11099_SHARP_)+"["+cljs.core.str.cljs$core$IFn$_invoke$arity$1(p2__11100_SHARP_)+"]");
}),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(sablono.core._STAR_group_STAR_,sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([name], 0))));
});
/**
* Create a field id from the supplied argument and current field group.
*/
sablono.core.make_id = (function make_id(name){return cljs.core.reduce.cljs$core$IFn$_invoke$arity$2((function (p1__11101_SHARP_,p2__11102_SHARP_){return (''+cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__11101_SHARP_)+"-"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(p2__11102_SHARP_));
}),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(sablono.core._STAR_group_STAR_,sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([name], 0))));
});
/**
* Creates a new <input> element.
*/
sablono.core.input_field = (function input_field(type,name,value){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$240,new cljs.core.PersistentArrayMap(null, 4, [cljs.core.constant$keyword$235,type,cljs.core.constant$keyword$251,sablono.core.make_name(name),cljs.core.constant$keyword$192,sablono.core.make_id(name),cljs.core.constant$keyword$183,value], null)], null);
});
/**
* Creates a hidden input field.
*/
sablono.core.hidden_field11103 = (function() {
var hidden_field11103 = null;
var hidden_field11103__1 = (function (name){return hidden_field11103.cljs$core$IFn$_invoke$arity$2(name,null);
});
var hidden_field11103__2 = (function (name,value){return sablono.core.input_field("hidden",name,value);
});
hidden_field11103 = function(name,value){
switch(arguments.length){
case 1:
return hidden_field11103__1.call(this,name);
case 2:
return hidden_field11103__2.call(this,name,value);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
hidden_field11103.cljs$core$IFn$_invoke$arity$1 = hidden_field11103__1;
hidden_field11103.cljs$core$IFn$_invoke$arity$2 = hidden_field11103__2;
return hidden_field11103;
})()
;
sablono.core.hidden_field = sablono.core.wrap_attrs(sablono.core.hidden_field11103);
/**
* Creates a new text input field.
*/
sablono.core.text_field11104 = (function() {
var text_field11104 = null;
var text_field11104__1 = (function (name){return text_field11104.cljs$core$IFn$_invoke$arity$2(name,null);
});
var text_field11104__2 = (function (name,value){return sablono.core.input_field("text",name,value);
});
text_field11104 = function(name,value){
switch(arguments.length){
case 1:
return text_field11104__1.call(this,name);
case 2:
return text_field11104__2.call(this,name,value);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
text_field11104.cljs$core$IFn$_invoke$arity$1 = text_field11104__1;
text_field11104.cljs$core$IFn$_invoke$arity$2 = text_field11104__2;
return text_field11104;
})()
;
sablono.core.text_field = sablono.core.wrap_attrs(sablono.core.text_field11104);
/**
* Creates a new password field.
*/
sablono.core.password_field11105 = (function() {
var password_field11105 = null;
var password_field11105__1 = (function (name){return password_field11105.cljs$core$IFn$_invoke$arity$2(name,null);
});
var password_field11105__2 = (function (name,value){return sablono.core.input_field("password",name,value);
});
password_field11105 = function(name,value){
switch(arguments.length){
case 1:
return password_field11105__1.call(this,name);
case 2:
return password_field11105__2.call(this,name,value);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
password_field11105.cljs$core$IFn$_invoke$arity$1 = password_field11105__1;
password_field11105.cljs$core$IFn$_invoke$arity$2 = password_field11105__2;
return password_field11105;
})()
;
sablono.core.password_field = sablono.core.wrap_attrs(sablono.core.password_field11105);
/**
* Creates a new email input field.
*/
sablono.core.email_field11106 = (function() {
var email_field11106 = null;
var email_field11106__1 = (function (name){return email_field11106.cljs$core$IFn$_invoke$arity$2(name,null);
});
var email_field11106__2 = (function (name,value){return sablono.core.input_field("email",name,value);
});
email_field11106 = function(name,value){
switch(arguments.length){
case 1:
return email_field11106__1.call(this,name);
case 2:
return email_field11106__2.call(this,name,value);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
email_field11106.cljs$core$IFn$_invoke$arity$1 = email_field11106__1;
email_field11106.cljs$core$IFn$_invoke$arity$2 = email_field11106__2;
return email_field11106;
})()
;
sablono.core.email_field = sablono.core.wrap_attrs(sablono.core.email_field11106);
/**
* Creates a check box.
*/
sablono.core.check_box11107 = (function() {
var check_box11107 = null;
var check_box11107__1 = (function (name){return check_box11107.cljs$core$IFn$_invoke$arity$2(name,null);
});
var check_box11107__2 = (function (name,checked_QMARK_){return check_box11107.cljs$core$IFn$_invoke$arity$3(name,checked_QMARK_,"true");
});
var check_box11107__3 = (function (name,checked_QMARK_,value){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$240,new cljs.core.PersistentArrayMap(null, 5, [cljs.core.constant$keyword$235,"checkbox",cljs.core.constant$keyword$251,sablono.core.make_name(name),cljs.core.constant$keyword$192,sablono.core.make_id(name),cljs.core.constant$keyword$183,value,cljs.core.constant$keyword$252,checked_QMARK_], null)], null);
});
check_box11107 = function(name,checked_QMARK_,value){
switch(arguments.length){
case 1:
return check_box11107__1.call(this,name);
case 2:
return check_box11107__2.call(this,name,checked_QMARK_);
case 3:
return check_box11107__3.call(this,name,checked_QMARK_,value);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
check_box11107.cljs$core$IFn$_invoke$arity$1 = check_box11107__1;
check_box11107.cljs$core$IFn$_invoke$arity$2 = check_box11107__2;
check_box11107.cljs$core$IFn$_invoke$arity$3 = check_box11107__3;
return check_box11107;
})()
;
sablono.core.check_box = sablono.core.wrap_attrs(sablono.core.check_box11107);
/**
* Creates a radio button.
*/
sablono.core.radio_button11108 = (function() {
var radio_button11108 = null;
var radio_button11108__1 = (function (group){return radio_button11108.cljs$core$IFn$_invoke$arity$2(group,null);
});
var radio_button11108__2 = (function (group,checked_QMARK_){return radio_button11108.cljs$core$IFn$_invoke$arity$3(group,checked_QMARK_,"true");
});
var radio_button11108__3 = (function (group,checked_QMARK_,value){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$240,new cljs.core.PersistentArrayMap(null, 5, [cljs.core.constant$keyword$235,"radio",cljs.core.constant$keyword$251,sablono.core.make_name(group),cljs.core.constant$keyword$192,sablono.core.make_id((''+cljs.core.str.cljs$core$IFn$_invoke$arity$1(sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([group], 0)))+"-"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(sablono.util.as_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.array_seq([value], 0))))),cljs.core.constant$keyword$183,value,cljs.core.constant$keyword$252,checked_QMARK_], null)], null);
});
radio_button11108 = function(group,checked_QMARK_,value){
switch(arguments.length){
case 1:
return radio_button11108__1.call(this,group);
case 2:
return radio_button11108__2.call(this,group,checked_QMARK_);
case 3:
return radio_button11108__3.call(this,group,checked_QMARK_,value);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
radio_button11108.cljs$core$IFn$_invoke$arity$1 = radio_button11108__1;
radio_button11108.cljs$core$IFn$_invoke$arity$2 = radio_button11108__2;
radio_button11108.cljs$core$IFn$_invoke$arity$3 = radio_button11108__3;
return radio_button11108;
})()
;
sablono.core.radio_button = sablono.core.wrap_attrs(sablono.core.radio_button11108);
/**
* Creates a seq of option tags from a collection.
*/
sablono.core.select_options11109 = (function() {
var select_options11109 = null;
var select_options11109__1 = (function (coll){return select_options11109.cljs$core$IFn$_invoke$arity$2(coll,null);
});
var select_options11109__2 = (function (coll,selected){var iter__4194__auto__ = (function iter__11118(s__11119){return (new cljs.core.LazySeq(null,(function (){var s__11119__$1 = s__11119;while(true){
var temp__4126__auto__ = cljs.core.seq(s__11119__$1);if(temp__4126__auto__)
{var s__11119__$2 = temp__4126__auto__;if(cljs.core.chunked_seq_QMARK_(s__11119__$2))
{var c__4192__auto__ = cljs.core.chunk_first(s__11119__$2);var size__4193__auto__ = cljs.core.count(c__4192__auto__);var b__11121 = cljs.core.chunk_buffer(size__4193__auto__);if((function (){var i__11120 = 0;while(true){
if((i__11120 < size__4193__auto__))
{var x = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4192__auto__,i__11120);cljs.core.chunk_append(b__11121,((cljs.core.sequential_QMARK_(x))?(function (){var vec__11124 = x;var text = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11124,0,null);var val = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11124,1,null);if(cljs.core.sequential_QMARK_(val))
{return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$253,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.constant$keyword$254,text], null),select_options11109.cljs$core$IFn$_invoke$arity$2(val,selected)], null);
} else
{return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$255,new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$183,val,cljs.core.constant$keyword$256,cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(val,selected)], null),text], null);
}
})():new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$255,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.constant$keyword$256,cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(x,selected)], null),x], null)));
{
var G__11126 = (i__11120 + 1);
i__11120 = G__11126;
continue;
}
} else
{return true;
}
break;
}
})())
{return cljs.core.chunk_cons(cljs.core.chunk(b__11121),iter__11118(cljs.core.chunk_rest(s__11119__$2)));
} else
{return cljs.core.chunk_cons(cljs.core.chunk(b__11121),null);
}
} else
{var x = cljs.core.first(s__11119__$2);return cljs.core.cons(((cljs.core.sequential_QMARK_(x))?(function (){var vec__11125 = x;var text = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11125,0,null);var val = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11125,1,null);if(cljs.core.sequential_QMARK_(val))
{return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$253,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.constant$keyword$254,text], null),select_options11109.cljs$core$IFn$_invoke$arity$2(val,selected)], null);
} else
{return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$255,new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$183,val,cljs.core.constant$keyword$256,cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(val,selected)], null),text], null);
}
})():new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$255,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.constant$keyword$256,cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(x,selected)], null),x], null)),iter__11118(cljs.core.rest(s__11119__$2)));
}
} else
{return null;
}
break;
}
}),null,null));
});return iter__4194__auto__(coll);
});
select_options11109 = function(coll,selected){
switch(arguments.length){
case 1:
return select_options11109__1.call(this,coll);
case 2:
return select_options11109__2.call(this,coll,selected);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
select_options11109.cljs$core$IFn$_invoke$arity$1 = select_options11109__1;
select_options11109.cljs$core$IFn$_invoke$arity$2 = select_options11109__2;
return select_options11109;
})()
;
sablono.core.select_options = sablono.core.wrap_attrs(sablono.core.select_options11109);
/**
* Creates a drop-down box using the <select> tag.
*/
sablono.core.drop_down11127 = (function() {
var drop_down11127 = null;
var drop_down11127__2 = (function (name,options){return drop_down11127.cljs$core$IFn$_invoke$arity$3(name,options,null);
});
var drop_down11127__3 = (function (name,options,selected){return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$257,new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$251,sablono.core.make_name(name),cljs.core.constant$keyword$192,sablono.core.make_id(name)], null),(sablono.core.select_options.cljs$core$IFn$_invoke$arity$2 ? sablono.core.select_options.cljs$core$IFn$_invoke$arity$2(options,selected) : sablono.core.select_options.call(null,options,selected))], null);
});
drop_down11127 = function(name,options,selected){
switch(arguments.length){
case 2:
return drop_down11127__2.call(this,name,options);
case 3:
return drop_down11127__3.call(this,name,options,selected);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
drop_down11127.cljs$core$IFn$_invoke$arity$2 = drop_down11127__2;
drop_down11127.cljs$core$IFn$_invoke$arity$3 = drop_down11127__3;
return drop_down11127;
})()
;
sablono.core.drop_down = sablono.core.wrap_attrs(sablono.core.drop_down11127);
/**
* Creates a text area element.
*/
sablono.core.text_area11128 = (function() {
var text_area11128 = null;
var text_area11128__1 = (function (name){return text_area11128.cljs$core$IFn$_invoke$arity$2(name,null);
});
var text_area11128__2 = (function (name,value){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$239,new cljs.core.PersistentArrayMap(null, 3, [cljs.core.constant$keyword$251,sablono.core.make_name(name),cljs.core.constant$keyword$192,sablono.core.make_id(name),cljs.core.constant$keyword$183,value], null)], null);
});
text_area11128 = function(name,value){
switch(arguments.length){
case 1:
return text_area11128__1.call(this,name);
case 2:
return text_area11128__2.call(this,name,value);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
text_area11128.cljs$core$IFn$_invoke$arity$1 = text_area11128__1;
text_area11128.cljs$core$IFn$_invoke$arity$2 = text_area11128__2;
return text_area11128;
})()
;
sablono.core.text_area = sablono.core.wrap_attrs(sablono.core.text_area11128);
/**
* Creates a file upload input.
*/
sablono.core.file_upload11129 = (function file_upload11129(name){return sablono.core.input_field("file",name,null);
});
sablono.core.file_upload = sablono.core.wrap_attrs(sablono.core.file_upload11129);
/**
* Creates a label for an input field with the supplied name.
*/
sablono.core.label11130 = (function label11130(name,text){return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$254,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.constant$keyword$237,sablono.core.make_id(name)], null),text], null);
});
sablono.core.label = sablono.core.wrap_attrs(sablono.core.label11130);
/**
* Creates a submit button.
*/
sablono.core.submit_button11131 = (function submit_button11131(text){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$240,new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$235,"submit",cljs.core.constant$keyword$183,text], null)], null);
});
sablono.core.submit_button = sablono.core.wrap_attrs(sablono.core.submit_button11131);
/**
* Creates a form reset button.
*/
sablono.core.reset_button11132 = (function reset_button11132(text){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$240,new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$235,"reset",cljs.core.constant$keyword$183,text], null)], null);
});
sablono.core.reset_button = sablono.core.wrap_attrs(sablono.core.reset_button11132);
/**
* Create a form that points to a particular method and route.
* e.g. (form-to [:put "/post"]
* ...)
* @param {...*} var_args
*/
sablono.core.form_to11133 = (function() { 
var form_to11133__delegate = function (p__11134,body){var vec__11136 = p__11134;var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11136,0,null);var action = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__11136,1,null);var method_str = clojure.string.upper_case(cljs.core.name(method));var action_uri = sablono.util.to_uri(action);return cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(((cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$258,null,cljs.core.constant$keyword$259,null], null), null),method))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$260,new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$213,method_str,cljs.core.constant$keyword$261,action_uri], null)], null):new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.constant$keyword$260,new cljs.core.PersistentArrayMap(null, 2, [cljs.core.constant$keyword$213,"POST",cljs.core.constant$keyword$261,action_uri], null),(sablono.core.hidden_field.cljs$core$IFn$_invoke$arity$2 ? sablono.core.hidden_field.cljs$core$IFn$_invoke$arity$2("_method",method_str) : sablono.core.hidden_field.call(null,"_method",method_str))], null)),body));
};
var form_to11133 = function (p__11134,var_args){
var body = null;if (arguments.length > 1) {
  body = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);} 
return form_to11133__delegate.call(this,p__11134,body);};
form_to11133.cljs$lang$maxFixedArity = 1;
form_to11133.cljs$lang$applyTo = (function (arglist__11137){
var p__11134 = cljs.core.first(arglist__11137);
var body = cljs.core.rest(arglist__11137);
return form_to11133__delegate(p__11134,body);
});
form_to11133.cljs$core$IFn$_invoke$arity$variadic = form_to11133__delegate;
return form_to11133;
})()
;
sablono.core.form_to = sablono.core.wrap_attrs(sablono.core.form_to11133);
