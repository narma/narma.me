// Compiled by ClojureScript 0.0-2227
goog.provide('kioo.reagent');
goog.require('cljs.core');
goog.require('kioo.util');
goog.require('reagent.impl.template');
goog.require('reagent.impl.template');
goog.require('kioo.util');
goog.require('kioo.util');
goog.require('kioo.core');
goog.require('kioo.core');
kioo.reagent.make_dom = (function make_dom(node){var rnode = ((cljs.core.map_QMARK_.call(null,node))?(function (){var c = new cljs.core.Keyword(null,"content","content",1965434859).cljs$core$IFn$_invoke$arity$1(node);if(cljs.core.vector_QMARK_.call(null,c))
{return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tag","tag",1014018828).cljs$core$IFn$_invoke$arity$1(node),new cljs.core.Keyword(null,"attrs","attrs",1107056660).cljs$core$IFn$_invoke$arity$1(node),reagent.impl.template.as_component.call(null,c)], null);
} else
{if(cljs.core.seq_QMARK_.call(null,c))
{return cljs.core.reduce.call(null,((function (c){
return (function (p1__8596_SHARP_,p2__8597_SHARP_){return cljs.core.conj.call(null,p1__8596_SHARP_,reagent.impl.template.as_component.call(null,p2__8597_SHARP_));
});})(c))
,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tag","tag",1014018828).cljs$core$IFn$_invoke$arity$1(node),new cljs.core.Keyword(null,"attrs","attrs",1107056660).cljs$core$IFn$_invoke$arity$1(node)], null),c);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tag","tag",1014018828).cljs$core$IFn$_invoke$arity$1(node),new cljs.core.Keyword(null,"attrs","attrs",1107056660).cljs$core$IFn$_invoke$arity$1(node),c], null);
} else
{return null;
}
}
}
})():node);return reagent.impl.template.as_component.call(null,rnode);
});
kioo.reagent.content = kioo.core.content;
kioo.reagent.append = kioo.core.append;
kioo.reagent.prepend = kioo.core.prepend;
/**
* @param {...*} var_args
*/
kioo.reagent.after = (function() { 
var after__delegate = function (body){return (function (node){return cljs.core.conj.call(null,body,node);
});
};
var after = function (var_args){
var body = null;if (arguments.length > 0) {
  body = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return after__delegate.call(this,body);};
after.cljs$lang$maxFixedArity = 0;
after.cljs$lang$applyTo = (function (arglist__8598){
var body = cljs.core.seq(arglist__8598);
return after__delegate(body);
});
after.cljs$core$IFn$_invoke$arity$variadic = after__delegate;
return after;
})()
;
/**
* @param {...*} var_args
*/
kioo.reagent.before = (function() { 
var before__delegate = function (body){return (function (node){return cljs.core.reduce.call(null,(function (p1__8599_SHARP_,p2__8600_SHARP_){return cljs.core.conj.call(null,p1__8599_SHARP_,p2__8600_SHARP_);
}),cljs.core._conj.call(null,cljs.core.List.EMPTY,node),body);
});
};
var before = function (var_args){
var body = null;if (arguments.length > 0) {
  body = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return before__delegate.call(this,body);};
before.cljs$lang$maxFixedArity = 0;
before.cljs$lang$applyTo = (function (arglist__8601){
var body = cljs.core.seq(arglist__8601);
return before__delegate(body);
});
before.cljs$core$IFn$_invoke$arity$variadic = before__delegate;
return before;
})()
;
kioo.reagent.substitute = kioo.core.substitute;
kioo.reagent.set_attr = kioo.core.set_attr;
kioo.reagent.remove_attr = kioo.core.remove_attr;
kioo.reagent.do__GT_ = kioo.core.do__GT_;
kioo.reagent.set_style = kioo.core.set_style;
kioo.reagent.remove_style = kioo.core.remove_style;
kioo.reagent.set_class = kioo.core.set_class;
kioo.reagent.add_class = kioo.core.add_class;
kioo.reagent.remove_class = kioo.core.remove_class;
kioo.reagent.wrap = (function wrap(tag,attrs){return (function (node){return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"tag","tag",1014018828),tag,new cljs.core.Keyword(null,"attrs","attrs",1107056660),attrs,new cljs.core.Keyword(null,"content","content",1965434859),kioo.reagent.make_dom.call(null,node)], null);
});
});
kioo.reagent.unwrap = kioo.core.unwrap;
kioo.reagent.html = kioo.core.html;
kioo.reagent.html_content = kioo.core.html_content;
kioo.reagent.listen = kioo.core.listen;
