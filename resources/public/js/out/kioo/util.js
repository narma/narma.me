// Compiled by ClojureScript 0.0-2227
goog.provide('kioo.util');
goog.require('cljs.core');
goog.require('clojure.string');
goog.require('clojure.string');
kioo.util._STAR_component_STAR_ = null;
/**
* Wrapper component used to mix-in lifecycle methods
* This was pulled from quiescent
*/
kioo.util.WrapComponent = React.createClass({"componentDidMount": (function (node){var this$ = this;var temp__4126__auto__ = (function (){var or__3469__auto__ = (this$.props["onMount"]);if(cljs.core.truth_(or__3469__auto__))
{return or__3469__auto__;
} else
{return (this$.props["onRender"]);
}
})();if(cljs.core.truth_(temp__4126__auto__))
{var f = temp__4126__auto__;return f.call(null,node);
} else
{return null;
}
}), "componentDidUpdate": (function (prev_props,prev_state,node){var this$ = this;var temp__4126__auto__ = (function (){var or__3469__auto__ = (this$.props["onUpdate"]);if(cljs.core.truth_(or__3469__auto__))
{return or__3469__auto__;
} else
{return (this$.props["onRender"]);
}
})();if(cljs.core.truth_(temp__4126__auto__))
{var f = temp__4126__auto__;var _STAR_component_STAR_8345 = kioo.util._STAR_component_STAR_;try{kioo.util._STAR_component_STAR_ = this$;
return f.call(null,node);
}finally {kioo.util._STAR_component_STAR_ = _STAR_component_STAR_8345;
}} else
{return null;
}
}), "render": (function (){var this$ = this;return (this$.props["wrappee"]);
})});
kioo.util.dont_camel_case = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["aria",null,"data",null], null), null);
kioo.util.camel_case = (function camel_case(dashed){if(typeof dashed === 'string')
{return dashed;
} else
{var name_str = cljs.core.name.call(null,dashed);var vec__8347 = clojure.string.split.call(null,name_str,/-/);var start = cljs.core.nth.call(null,vec__8347,0,null);var parts = cljs.core.nthnext.call(null,vec__8347,1);if(cljs.core.truth_(kioo.util.dont_camel_case.call(null,start)))
{return name_str;
} else
{return cljs.core.apply.call(null,cljs.core.str,start,cljs.core.map.call(null,clojure.string.capitalize,parts));
}
}
});
kioo.util.attribute_map = cljs.core.assoc.call(null,cljs.core.reduce.call(null,(function (p1__8348_SHARP_,p2__8349_SHARP_){return cljs.core.assoc.call(null,p1__8348_SHARP_,cljs.core.keyword.call(null,cljs.core.name.call(null,p2__8349_SHARP_).toLowerCase()),p2__8349_SHARP_);
}),cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentVector.fromArray([new cljs.core.Keyword(null,"accessKey","accessKey",3193813453),new cljs.core.Keyword(null,"allowFullScreen","allowFullScreen",4370862966),new cljs.core.Keyword(null,"allowTransparency","allowTransparency",1093898419),new cljs.core.Keyword(null,"autoComplete","autoComplete",4535568250),new cljs.core.Keyword(null,"autoFocus","autoFocus",2651959259),new cljs.core.Keyword(null,"autoPlay","autoPlay",2452513013),new cljs.core.Keyword(null,"cellPadding","cellPadding",3303033793),new cljs.core.Keyword(null,"cellSpacing","cellSpacing",2097214451),new cljs.core.Keyword(null,"charSet","charSet",1752947870),new cljs.core.Keyword(null,"colSpan","colSpan",1962614876),new cljs.core.Keyword(null,"contentEditable","contentEditable",745009519),new cljs.core.Keyword(null,"contextMenu","contextMenu",910836512),new cljs.core.Keyword(null,"dateTime","dateTime",2806653709),new cljs.core.Keyword(null,"encType","encType",3700720294),new cljs.core.Keyword(null,"formNoValidate","formNoValidate",3746099533),new cljs.core.Keyword(null,"frameBorder","frameBorder",4678186859),new cljs.core.Keyword(null,"htmlFor","htmlFor",2249940112),new cljs.core.Keyword(null,"httpEquiv","httpEquiv",1169651712),new cljs.core.Keyword(null,"maxLength","maxLength",4517471452),new cljs.core.Keyword(null,"noValidate","noValidate",751644009),new cljs.core.Keyword(null,"radioGroup","radioGroup",989670070),new cljs.core.Keyword(null,"readOnly","readOnly",4441187796),new cljs.core.Keyword(null,"rowSpan","rowSpan",2400426934),new cljs.core.Keyword(null,"scrollLeft","scrollLeft",1079951334),new cljs.core.Keyword(null,"scrollTop","scrollTop",1431684794),new cljs.core.Keyword(null,"spellCheck","spellCheck",3766575154),new cljs.core.Keyword(null,"srcDoc","srcDoc",4414556198),new cljs.core.Keyword(null,"tabIndex","tabIndex",4381401519),new cljs.core.Keyword(null,"gradientTransform","gradientTransform",2836569486),new cljs.core.Keyword(null,"gradientUnits","gradientUnits",3376636305),new cljs.core.Keyword(null,"spreadMethod","spreadMethod",2892660646),new cljs.core.Keyword(null,"stopColor","stopColor",2618022899),new cljs.core.Keyword(null,"stopOpacity","stopOpacity",2756891931),new cljs.core.Keyword(null,"strokeLinecap","strokeLinecap",2041479544),new cljs.core.Keyword(null,"strokeWidth","strokeWidth",2937970144),new cljs.core.Keyword(null,"textAnchor","textAnchor",2344456628),new cljs.core.Keyword(null,"viewBox","viewBox",1468104792)], true)),new cljs.core.Keyword(null,"class","class",1108647146),new cljs.core.Keyword(null,"className","className",1004015509));
kioo.util.transform_keys = (function transform_keys(attrs){return cljs.core.reduce.call(null,(function (m,p__8352){var vec__8353 = p__8352;var k = cljs.core.nth.call(null,vec__8353,0,null);var v = cljs.core.nth.call(null,vec__8353,1,null);return cljs.core.assoc.call(null,m,(function (){var or__3469__auto__ = kioo.util.attribute_map.call(null,k);if(cljs.core.truth_(or__3469__auto__))
{return or__3469__auto__;
} else
{return k;
}
})(),v);
}),cljs.core.PersistentArrayMap.EMPTY,attrs);
});
kioo.util.convert_attrs = (function convert_attrs(attrs){var style = (cljs.core.truth_(new cljs.core.Keyword(null,"style","style",1123684643).cljs$core$IFn$_invoke$arity$1(attrs))?(function (){var vals = cljs.core.re_seq.call(null,/\s*([^:;]*)[:][\s]*([^;]+)/,new cljs.core.Keyword(null,"style","style",1123684643).cljs$core$IFn$_invoke$arity$1(attrs));return cljs.core.reduce.call(null,((function (vals){
return (function (m,p__8356){var vec__8357 = p__8356;var _ = cljs.core.nth.call(null,vec__8357,0,null);var k = cljs.core.nth.call(null,vec__8357,1,null);var v = cljs.core.nth.call(null,vec__8357,2,null);return cljs.core.assoc.call(null,m,k,v.trim());
});})(vals))
,cljs.core.PersistentArrayMap.EMPTY,vals);
})():null);var class_name = new cljs.core.Keyword(null,"class","class",1108647146).cljs$core$IFn$_invoke$arity$1(attrs);return cljs.core.assoc.call(null,kioo.util.transform_keys.call(null,attrs),new cljs.core.Keyword(null,"style","style",1123684643),style);
});
kioo.util.flatten_nodes = (function flatten_nodes(nodes){return cljs.core.reduce.call(null,(function (p1__8359_SHARP_,p2__8358_SHARP_){if(cljs.core.seq_QMARK_.call(null,p2__8358_SHARP_))
{return cljs.core.concat.call(null,p2__8358_SHARP_,p1__8359_SHARP_);
} else
{return cljs.core.conj.call(null,p1__8359_SHARP_,p2__8358_SHARP_);
}
}),cljs.core.List.EMPTY,cljs.core.reverse.call(null,nodes));
});
/**
* Returns a regular expression that matches the HTML attribute `attr`
* and it's value.
*/
kioo.util.attr_pattern = (function attr_pattern(attr){return cljs.core.re_pattern.call(null,("\\s+"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.name.call(null,attr))+"\\s*=\\s*['\"][^\"']+['\"]"));
});
/**
* Strip the HTML attribute `attr` and it's value from the string `s`.
*/
kioo.util.strip_attr = (function strip_attr(s,attr){if(cljs.core.truth_(s))
{return clojure.string.replace.call(null,s,kioo.util.attr_pattern.call(null,attr),"");
} else
{return null;
}
});
