// Compiled by ClojureScript 0.0-2227
goog.provide('sablono.core');
goog.require('cljs.core');
goog.require('clojure.walk');
goog.require('clojure.string');
goog.require('sablono.util');
goog.require('goog.dom');
goog.require('goog.dom');
goog.require('sablono.interpreter');
goog.require('sablono.interpreter');
goog.require('sablono.util');
goog.require('clojure.walk');
goog.require('clojure.string');
/**
* Add an optional attribute argument to a function that returns a element vector.
*/
sablono.core.wrap_attrs = (function wrap_attrs(func){return (function() { 
var G__8451__delegate = function (args){if(cljs.core.map_QMARK_.call(null,cljs.core.first.call(null,args)))
{var vec__8450 = cljs.core.apply.call(null,func,cljs.core.rest.call(null,args));var tag = cljs.core.nth.call(null,vec__8450,0,null);var body = cljs.core.nthnext.call(null,vec__8450,1);if(cljs.core.map_QMARK_.call(null,cljs.core.first.call(null,body)))
{return cljs.core.apply.call(null,cljs.core.vector,tag,cljs.core.merge.call(null,cljs.core.first.call(null,body),cljs.core.first.call(null,args)),cljs.core.rest.call(null,body));
} else
{return cljs.core.apply.call(null,cljs.core.vector,tag,cljs.core.first.call(null,args),body);
}
} else
{return cljs.core.apply.call(null,func,args);
}
};
var G__8451 = function (var_args){
var args = null;if (arguments.length > 0) {
  args = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return G__8451__delegate.call(this,args);};
G__8451.cljs$lang$maxFixedArity = 0;
G__8451.cljs$lang$applyTo = (function (arglist__8452){
var args = cljs.core.seq(arglist__8452);
return G__8451__delegate(args);
});
G__8451.cljs$core$IFn$_invoke$arity$variadic = G__8451__delegate;
return G__8451;
})()
;
});
sablono.core.update_arglists = (function update_arglists(arglists){var iter__4194__auto__ = (function iter__8457(s__8458){return (new cljs.core.LazySeq(null,(function (){var s__8458__$1 = s__8458;while(true){
var temp__4126__auto__ = cljs.core.seq.call(null,s__8458__$1);if(temp__4126__auto__)
{var s__8458__$2 = temp__4126__auto__;if(cljs.core.chunked_seq_QMARK_.call(null,s__8458__$2))
{var c__4192__auto__ = cljs.core.chunk_first.call(null,s__8458__$2);var size__4193__auto__ = cljs.core.count.call(null,c__4192__auto__);var b__8460 = cljs.core.chunk_buffer.call(null,size__4193__auto__);if((function (){var i__8459 = 0;while(true){
if((i__8459 < size__4193__auto__))
{var args = cljs.core._nth.call(null,c__4192__auto__,i__8459);cljs.core.chunk_append.call(null,b__8460,cljs.core.vec.call(null,cljs.core.cons.call(null,new cljs.core.Symbol(null,"attr-map?","attr-map?",-1682549128,null),args)));
{
var G__8461 = (i__8459 + 1);
i__8459 = G__8461;
continue;
}
} else
{return true;
}
break;
}
})())
{return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__8460),iter__8457.call(null,cljs.core.chunk_rest.call(null,s__8458__$2)));
} else
{return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__8460),null);
}
} else
{var args = cljs.core.first.call(null,s__8458__$2);return cljs.core.cons.call(null,cljs.core.vec.call(null,cljs.core.cons.call(null,new cljs.core.Symbol(null,"attr-map?","attr-map?",-1682549128,null),args)),iter__8457.call(null,cljs.core.rest.call(null,s__8458__$2)));
}
} else
{return null;
}
break;
}
}),null,null));
});return iter__4194__auto__.call(null,arglists);
});
/**
* Render the React `component` as an HTML string.
*/
sablono.core.render = (function render(component){var html = cljs.core.atom.call(null,null);React.renderComponentToString(component,((function (html){
return (function (p1__8462_SHARP_){return cljs.core.reset_BANG_.call(null,html,p1__8462_SHARP_);
});})(html))
);
return cljs.core.deref.call(null,html);
});
/**
* Include a list of external stylesheet files.
* @param {...*} var_args
*/
sablono.core.include_css = (function() { 
var include_css__delegate = function (styles){var iter__4194__auto__ = (function iter__8467(s__8468){return (new cljs.core.LazySeq(null,(function (){var s__8468__$1 = s__8468;while(true){
var temp__4126__auto__ = cljs.core.seq.call(null,s__8468__$1);if(temp__4126__auto__)
{var s__8468__$2 = temp__4126__auto__;if(cljs.core.chunked_seq_QMARK_.call(null,s__8468__$2))
{var c__4192__auto__ = cljs.core.chunk_first.call(null,s__8468__$2);var size__4193__auto__ = cljs.core.count.call(null,c__4192__auto__);var b__8470 = cljs.core.chunk_buffer.call(null,size__4193__auto__);if((function (){var i__8469 = 0;while(true){
if((i__8469 < size__4193__auto__))
{var style = cljs.core._nth.call(null,c__4192__auto__,i__8469);cljs.core.chunk_append.call(null,b__8470,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"link","link",1017226092),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1017479852),"text/css",new cljs.core.Keyword(null,"href","href",1017115293),sablono.util.as_str.call(null,style),new cljs.core.Keyword(null,"rel","rel",1014017035),"stylesheet"], null)], null));
{
var G__8471 = (i__8469 + 1);
i__8469 = G__8471;
continue;
}
} else
{return true;
}
break;
}
})())
{return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__8470),iter__8467.call(null,cljs.core.chunk_rest.call(null,s__8468__$2)));
} else
{return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__8470),null);
}
} else
{var style = cljs.core.first.call(null,s__8468__$2);return cljs.core.cons.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"link","link",1017226092),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1017479852),"text/css",new cljs.core.Keyword(null,"href","href",1017115293),sablono.util.as_str.call(null,style),new cljs.core.Keyword(null,"rel","rel",1014017035),"stylesheet"], null)], null),iter__8467.call(null,cljs.core.rest.call(null,s__8468__$2)));
}
} else
{return null;
}
break;
}
}),null,null));
});return iter__4194__auto__.call(null,styles);
};
var include_css = function (var_args){
var styles = null;if (arguments.length > 0) {
  styles = cljs.core.array_seq(Array.prototype.slice.call(arguments, 0),0);} 
return include_css__delegate.call(this,styles);};
include_css.cljs$lang$maxFixedArity = 0;
include_css.cljs$lang$applyTo = (function (arglist__8472){
var styles = cljs.core.seq(arglist__8472);
return include_css__delegate(styles);
});
include_css.cljs$core$IFn$_invoke$arity$variadic = include_css__delegate;
return include_css;
})()
;
/**
* Include the JavaScript library at `src`.
*/
sablono.core.include_js = (function include_js(src){return goog.dom.appendChild(goog.dom.getDocument().body,goog.dom.createDom("script",{"src": src}));
});
/**
* Include Facebook's React JavaScript library.
*/
sablono.core.include_react = (function include_react(){return sablono.core.include_js.call(null,"http://fb.me/react-0.8.0.js");
});
/**
* Wraps some content in a HTML hyperlink with the supplied URL.
* @param {...*} var_args
*/
sablono.core.link_to8473 = (function() { 
var link_to8473__delegate = function (url,content){return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",1013904339),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",1017115293),sablono.util.as_str.call(null,url)], null),content], null);
};
var link_to8473 = function (url,var_args){
var content = null;if (arguments.length > 1) {
  content = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);} 
return link_to8473__delegate.call(this,url,content);};
link_to8473.cljs$lang$maxFixedArity = 1;
link_to8473.cljs$lang$applyTo = (function (arglist__8474){
var url = cljs.core.first(arglist__8474);
var content = cljs.core.rest(arglist__8474);
return link_to8473__delegate(url,content);
});
link_to8473.cljs$core$IFn$_invoke$arity$variadic = link_to8473__delegate;
return link_to8473;
})()
;
sablono.core.link_to = sablono.core.wrap_attrs.call(null,sablono.core.link_to8473);
/**
* Wraps some content in a HTML hyperlink with the supplied e-mail
* address. If no content provided use the e-mail address as content.
* @param {...*} var_args
*/
sablono.core.mail_to8475 = (function() { 
var mail_to8475__delegate = function (e_mail,p__8476){var vec__8478 = p__8476;var content = cljs.core.nth.call(null,vec__8478,0,null);return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",1013904339),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",1017115293),("mailto:"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_mail))], null),(function (){var or__3469__auto__ = content;if(cljs.core.truth_(or__3469__auto__))
{return or__3469__auto__;
} else
{return e_mail;
}
})()], null);
};
var mail_to8475 = function (e_mail,var_args){
var p__8476 = null;if (arguments.length > 1) {
  p__8476 = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);} 
return mail_to8475__delegate.call(this,e_mail,p__8476);};
mail_to8475.cljs$lang$maxFixedArity = 1;
mail_to8475.cljs$lang$applyTo = (function (arglist__8479){
var e_mail = cljs.core.first(arglist__8479);
var p__8476 = cljs.core.rest(arglist__8479);
return mail_to8475__delegate(e_mail,p__8476);
});
mail_to8475.cljs$core$IFn$_invoke$arity$variadic = mail_to8475__delegate;
return mail_to8475;
})()
;
sablono.core.mail_to = sablono.core.wrap_attrs.call(null,sablono.core.mail_to8475);
/**
* Wrap a collection in an unordered list.
*/
sablono.core.unordered_list8480 = (function unordered_list8480(coll){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul","ul",1013907977),(function (){var iter__4194__auto__ = (function iter__8485(s__8486){return (new cljs.core.LazySeq(null,(function (){var s__8486__$1 = s__8486;while(true){
var temp__4126__auto__ = cljs.core.seq.call(null,s__8486__$1);if(temp__4126__auto__)
{var s__8486__$2 = temp__4126__auto__;if(cljs.core.chunked_seq_QMARK_.call(null,s__8486__$2))
{var c__4192__auto__ = cljs.core.chunk_first.call(null,s__8486__$2);var size__4193__auto__ = cljs.core.count.call(null,c__4192__auto__);var b__8488 = cljs.core.chunk_buffer.call(null,size__4193__auto__);if((function (){var i__8487 = 0;while(true){
if((i__8487 < size__4193__auto__))
{var x = cljs.core._nth.call(null,c__4192__auto__,i__8487);cljs.core.chunk_append.call(null,b__8488,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",1013907695),x], null));
{
var G__8489 = (i__8487 + 1);
i__8487 = G__8489;
continue;
}
} else
{return true;
}
break;
}
})())
{return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__8488),iter__8485.call(null,cljs.core.chunk_rest.call(null,s__8486__$2)));
} else
{return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__8488),null);
}
} else
{var x = cljs.core.first.call(null,s__8486__$2);return cljs.core.cons.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",1013907695),x], null),iter__8485.call(null,cljs.core.rest.call(null,s__8486__$2)));
}
} else
{return null;
}
break;
}
}),null,null));
});return iter__4194__auto__.call(null,coll);
})()], null);
});
sablono.core.unordered_list = sablono.core.wrap_attrs.call(null,sablono.core.unordered_list8480);
/**
* Wrap a collection in an ordered list.
*/
sablono.core.ordered_list8490 = (function ordered_list8490(coll){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ol","ol",1013907791),(function (){var iter__4194__auto__ = (function iter__8495(s__8496){return (new cljs.core.LazySeq(null,(function (){var s__8496__$1 = s__8496;while(true){
var temp__4126__auto__ = cljs.core.seq.call(null,s__8496__$1);if(temp__4126__auto__)
{var s__8496__$2 = temp__4126__auto__;if(cljs.core.chunked_seq_QMARK_.call(null,s__8496__$2))
{var c__4192__auto__ = cljs.core.chunk_first.call(null,s__8496__$2);var size__4193__auto__ = cljs.core.count.call(null,c__4192__auto__);var b__8498 = cljs.core.chunk_buffer.call(null,size__4193__auto__);if((function (){var i__8497 = 0;while(true){
if((i__8497 < size__4193__auto__))
{var x = cljs.core._nth.call(null,c__4192__auto__,i__8497);cljs.core.chunk_append.call(null,b__8498,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",1013907695),x], null));
{
var G__8499 = (i__8497 + 1);
i__8497 = G__8499;
continue;
}
} else
{return true;
}
break;
}
})())
{return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__8498),iter__8495.call(null,cljs.core.chunk_rest.call(null,s__8496__$2)));
} else
{return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__8498),null);
}
} else
{var x = cljs.core.first.call(null,s__8496__$2);return cljs.core.cons.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",1013907695),x], null),iter__8495.call(null,cljs.core.rest.call(null,s__8496__$2)));
}
} else
{return null;
}
break;
}
}),null,null));
});return iter__4194__auto__.call(null,coll);
})()], null);
});
sablono.core.ordered_list = sablono.core.wrap_attrs.call(null,sablono.core.ordered_list8490);
/**
* Create an image element.
*/
sablono.core.image8500 = (function() {
var image8500 = null;
var image8500__1 = (function (src){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"img","img",1014008629),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"src","src",1014018390),sablono.util.as_str.call(null,src)], null)], null);
});
var image8500__2 = (function (src,alt){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"img","img",1014008629),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"src","src",1014018390),sablono.util.as_str.call(null,src),new cljs.core.Keyword(null,"alt","alt",1014000923),alt], null)], null);
});
image8500 = function(src,alt){
switch(arguments.length){
case 1:
return image8500__1.call(this,src);
case 2:
return image8500__2.call(this,src,alt);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
image8500.cljs$core$IFn$_invoke$arity$1 = image8500__1;
image8500.cljs$core$IFn$_invoke$arity$2 = image8500__2;
return image8500;
})()
;
sablono.core.image = sablono.core.wrap_attrs.call(null,sablono.core.image8500);
sablono.core._STAR_group_STAR_ = cljs.core.PersistentVector.EMPTY;
/**
* Create a field name from the supplied argument the current field group.
*/
sablono.core.make_name = (function make_name(name){return cljs.core.reduce.call(null,(function (p1__8501_SHARP_,p2__8502_SHARP_){return (''+cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__8501_SHARP_)+"["+cljs.core.str.cljs$core$IFn$_invoke$arity$1(p2__8502_SHARP_)+"]");
}),cljs.core.conj.call(null,sablono.core._STAR_group_STAR_,sablono.util.as_str.call(null,name)));
});
/**
* Create a field id from the supplied argument and current field group.
*/
sablono.core.make_id = (function make_id(name){return cljs.core.reduce.call(null,(function (p1__8503_SHARP_,p2__8504_SHARP_){return (''+cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__8503_SHARP_)+"-"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(p2__8504_SHARP_));
}),cljs.core.conj.call(null,sablono.core._STAR_group_STAR_,sablono.util.as_str.call(null,name)));
});
/**
* Creates a new <input> element.
*/
sablono.core.input_field = (function input_field(type,name,value){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",1114262332),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1017479852),type,new cljs.core.Keyword(null,"name","name",1017277949),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",1013907597),sablono.core.make_id.call(null,name),new cljs.core.Keyword(null,"value","value",1125876963),value], null)], null);
});
/**
* Creates a hidden input field.
*/
sablono.core.hidden_field8505 = (function() {
var hidden_field8505 = null;
var hidden_field8505__1 = (function (name){return hidden_field8505.call(null,name,null);
});
var hidden_field8505__2 = (function (name,value){return sablono.core.input_field.call(null,"hidden",name,value);
});
hidden_field8505 = function(name,value){
switch(arguments.length){
case 1:
return hidden_field8505__1.call(this,name);
case 2:
return hidden_field8505__2.call(this,name,value);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
hidden_field8505.cljs$core$IFn$_invoke$arity$1 = hidden_field8505__1;
hidden_field8505.cljs$core$IFn$_invoke$arity$2 = hidden_field8505__2;
return hidden_field8505;
})()
;
sablono.core.hidden_field = sablono.core.wrap_attrs.call(null,sablono.core.hidden_field8505);
/**
* Creates a new text input field.
*/
sablono.core.text_field8506 = (function() {
var text_field8506 = null;
var text_field8506__1 = (function (name){return text_field8506.call(null,name,null);
});
var text_field8506__2 = (function (name,value){return sablono.core.input_field.call(null,"text",name,value);
});
text_field8506 = function(name,value){
switch(arguments.length){
case 1:
return text_field8506__1.call(this,name);
case 2:
return text_field8506__2.call(this,name,value);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
text_field8506.cljs$core$IFn$_invoke$arity$1 = text_field8506__1;
text_field8506.cljs$core$IFn$_invoke$arity$2 = text_field8506__2;
return text_field8506;
})()
;
sablono.core.text_field = sablono.core.wrap_attrs.call(null,sablono.core.text_field8506);
/**
* Creates a new password field.
*/
sablono.core.password_field8507 = (function() {
var password_field8507 = null;
var password_field8507__1 = (function (name){return password_field8507.call(null,name,null);
});
var password_field8507__2 = (function (name,value){return sablono.core.input_field.call(null,"password",name,value);
});
password_field8507 = function(name,value){
switch(arguments.length){
case 1:
return password_field8507__1.call(this,name);
case 2:
return password_field8507__2.call(this,name,value);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
password_field8507.cljs$core$IFn$_invoke$arity$1 = password_field8507__1;
password_field8507.cljs$core$IFn$_invoke$arity$2 = password_field8507__2;
return password_field8507;
})()
;
sablono.core.password_field = sablono.core.wrap_attrs.call(null,sablono.core.password_field8507);
/**
* Creates a new email input field.
*/
sablono.core.email_field8508 = (function() {
var email_field8508 = null;
var email_field8508__1 = (function (name){return email_field8508.call(null,name,null);
});
var email_field8508__2 = (function (name,value){return sablono.core.input_field.call(null,"email",name,value);
});
email_field8508 = function(name,value){
switch(arguments.length){
case 1:
return email_field8508__1.call(this,name);
case 2:
return email_field8508__2.call(this,name,value);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
email_field8508.cljs$core$IFn$_invoke$arity$1 = email_field8508__1;
email_field8508.cljs$core$IFn$_invoke$arity$2 = email_field8508__2;
return email_field8508;
})()
;
sablono.core.email_field = sablono.core.wrap_attrs.call(null,sablono.core.email_field8508);
/**
* Creates a check box.
*/
sablono.core.check_box8509 = (function() {
var check_box8509 = null;
var check_box8509__1 = (function (name){return check_box8509.call(null,name,null);
});
var check_box8509__2 = (function (name,checked_QMARK_){return check_box8509.call(null,name,checked_QMARK_,"true");
});
var check_box8509__3 = (function (name,checked_QMARK_,value){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",1114262332),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"type","type",1017479852),"checkbox",new cljs.core.Keyword(null,"name","name",1017277949),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",1013907597),sablono.core.make_id.call(null,name),new cljs.core.Keyword(null,"value","value",1125876963),value,new cljs.core.Keyword(null,"checked","checked",1756218137),checked_QMARK_], null)], null);
});
check_box8509 = function(name,checked_QMARK_,value){
switch(arguments.length){
case 1:
return check_box8509__1.call(this,name);
case 2:
return check_box8509__2.call(this,name,checked_QMARK_);
case 3:
return check_box8509__3.call(this,name,checked_QMARK_,value);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
check_box8509.cljs$core$IFn$_invoke$arity$1 = check_box8509__1;
check_box8509.cljs$core$IFn$_invoke$arity$2 = check_box8509__2;
check_box8509.cljs$core$IFn$_invoke$arity$3 = check_box8509__3;
return check_box8509;
})()
;
sablono.core.check_box = sablono.core.wrap_attrs.call(null,sablono.core.check_box8509);
/**
* Creates a radio button.
*/
sablono.core.radio_button8510 = (function() {
var radio_button8510 = null;
var radio_button8510__1 = (function (group){return radio_button8510.call(null,group,null);
});
var radio_button8510__2 = (function (group,checked_QMARK_){return radio_button8510.call(null,group,checked_QMARK_,"true");
});
var radio_button8510__3 = (function (group,checked_QMARK_,value){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",1114262332),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"type","type",1017479852),"radio",new cljs.core.Keyword(null,"name","name",1017277949),sablono.core.make_name.call(null,group),new cljs.core.Keyword(null,"id","id",1013907597),sablono.core.make_id.call(null,(''+cljs.core.str.cljs$core$IFn$_invoke$arity$1(sablono.util.as_str.call(null,group))+"-"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(sablono.util.as_str.call(null,value)))),new cljs.core.Keyword(null,"value","value",1125876963),value,new cljs.core.Keyword(null,"checked","checked",1756218137),checked_QMARK_], null)], null);
});
radio_button8510 = function(group,checked_QMARK_,value){
switch(arguments.length){
case 1:
return radio_button8510__1.call(this,group);
case 2:
return radio_button8510__2.call(this,group,checked_QMARK_);
case 3:
return radio_button8510__3.call(this,group,checked_QMARK_,value);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
radio_button8510.cljs$core$IFn$_invoke$arity$1 = radio_button8510__1;
radio_button8510.cljs$core$IFn$_invoke$arity$2 = radio_button8510__2;
radio_button8510.cljs$core$IFn$_invoke$arity$3 = radio_button8510__3;
return radio_button8510;
})()
;
sablono.core.radio_button = sablono.core.wrap_attrs.call(null,sablono.core.radio_button8510);
/**
* Creates a seq of option tags from a collection.
*/
sablono.core.select_options8511 = (function() {
var select_options8511 = null;
var select_options8511__1 = (function (coll){return select_options8511.call(null,coll,null);
});
var select_options8511__2 = (function (coll,selected){var iter__4194__auto__ = (function iter__8520(s__8521){return (new cljs.core.LazySeq(null,(function (){var s__8521__$1 = s__8521;while(true){
var temp__4126__auto__ = cljs.core.seq.call(null,s__8521__$1);if(temp__4126__auto__)
{var s__8521__$2 = temp__4126__auto__;if(cljs.core.chunked_seq_QMARK_.call(null,s__8521__$2))
{var c__4192__auto__ = cljs.core.chunk_first.call(null,s__8521__$2);var size__4193__auto__ = cljs.core.count.call(null,c__4192__auto__);var b__8523 = cljs.core.chunk_buffer.call(null,size__4193__auto__);if((function (){var i__8522 = 0;while(true){
if((i__8522 < size__4193__auto__))
{var x = cljs.core._nth.call(null,c__4192__auto__,i__8522);cljs.core.chunk_append.call(null,b__8523,((cljs.core.sequential_QMARK_.call(null,x))?(function (){var vec__8526 = x;var text = cljs.core.nth.call(null,vec__8526,0,null);var val = cljs.core.nth.call(null,vec__8526,1,null);if(cljs.core.sequential_QMARK_.call(null,val))
{return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"optgroup","optgroup",933131038),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"label","label",1116631654),text], null),select_options8511.call(null,val,selected)], null);
} else
{return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",4298734567),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"value","value",1125876963),val,new cljs.core.Keyword(null,"selected","selected",2205476365),cljs.core._EQ_.call(null,val,selected)], null),text], null);
}
})():new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",4298734567),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"selected","selected",2205476365),cljs.core._EQ_.call(null,x,selected)], null),x], null)));
{
var G__8528 = (i__8522 + 1);
i__8522 = G__8528;
continue;
}
} else
{return true;
}
break;
}
})())
{return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__8523),iter__8520.call(null,cljs.core.chunk_rest.call(null,s__8521__$2)));
} else
{return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__8523),null);
}
} else
{var x = cljs.core.first.call(null,s__8521__$2);return cljs.core.cons.call(null,((cljs.core.sequential_QMARK_.call(null,x))?(function (){var vec__8527 = x;var text = cljs.core.nth.call(null,vec__8527,0,null);var val = cljs.core.nth.call(null,vec__8527,1,null);if(cljs.core.sequential_QMARK_.call(null,val))
{return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"optgroup","optgroup",933131038),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"label","label",1116631654),text], null),select_options8511.call(null,val,selected)], null);
} else
{return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",4298734567),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"value","value",1125876963),val,new cljs.core.Keyword(null,"selected","selected",2205476365),cljs.core._EQ_.call(null,val,selected)], null),text], null);
}
})():new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",4298734567),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"selected","selected",2205476365),cljs.core._EQ_.call(null,x,selected)], null),x], null)),iter__8520.call(null,cljs.core.rest.call(null,s__8521__$2)));
}
} else
{return null;
}
break;
}
}),null,null));
});return iter__4194__auto__.call(null,coll);
});
select_options8511 = function(coll,selected){
switch(arguments.length){
case 1:
return select_options8511__1.call(this,coll);
case 2:
return select_options8511__2.call(this,coll,selected);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
select_options8511.cljs$core$IFn$_invoke$arity$1 = select_options8511__1;
select_options8511.cljs$core$IFn$_invoke$arity$2 = select_options8511__2;
return select_options8511;
})()
;
sablono.core.select_options = sablono.core.wrap_attrs.call(null,sablono.core.select_options8511);
/**
* Creates a drop-down box using the <select> tag.
*/
sablono.core.drop_down8529 = (function() {
var drop_down8529 = null;
var drop_down8529__2 = (function (name,options){return drop_down8529.call(null,name,options,null);
});
var drop_down8529__3 = (function (name,options,selected){return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"select","select",4402849902),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"name","name",1017277949),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",1013907597),sablono.core.make_id.call(null,name)], null),sablono.core.select_options.call(null,options,selected)], null);
});
drop_down8529 = function(name,options,selected){
switch(arguments.length){
case 2:
return drop_down8529__2.call(this,name,options);
case 3:
return drop_down8529__3.call(this,name,options,selected);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
drop_down8529.cljs$core$IFn$_invoke$arity$2 = drop_down8529__2;
drop_down8529.cljs$core$IFn$_invoke$arity$3 = drop_down8529__3;
return drop_down8529;
})()
;
sablono.core.drop_down = sablono.core.wrap_attrs.call(null,sablono.core.drop_down8529);
/**
* Creates a text area element.
*/
sablono.core.text_area8530 = (function() {
var text_area8530 = null;
var text_area8530__1 = (function (name){return text_area8530.call(null,name,null);
});
var text_area8530__2 = (function (name,value){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"textarea","textarea",4305627820),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"name","name",1017277949),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",1013907597),sablono.core.make_id.call(null,name),new cljs.core.Keyword(null,"value","value",1125876963),value], null)], null);
});
text_area8530 = function(name,value){
switch(arguments.length){
case 1:
return text_area8530__1.call(this,name);
case 2:
return text_area8530__2.call(this,name,value);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
text_area8530.cljs$core$IFn$_invoke$arity$1 = text_area8530__1;
text_area8530.cljs$core$IFn$_invoke$arity$2 = text_area8530__2;
return text_area8530;
})()
;
sablono.core.text_area = sablono.core.wrap_attrs.call(null,sablono.core.text_area8530);
/**
* Creates a file upload input.
*/
sablono.core.file_upload8531 = (function file_upload8531(name){return sablono.core.input_field.call(null,"file",name,null);
});
sablono.core.file_upload = sablono.core.wrap_attrs.call(null,sablono.core.file_upload8531);
/**
* Creates a label for an input field with the supplied name.
*/
sablono.core.label8532 = (function label8532(name,text){return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label","label",1116631654),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"htmlFor","htmlFor",2249940112),sablono.core.make_id.call(null,name)], null),text], null);
});
sablono.core.label = sablono.core.wrap_attrs.call(null,sablono.core.label8532);
/**
* Creates a submit button.
*/
sablono.core.submit_button8533 = (function submit_button8533(text){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",1114262332),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1017479852),"submit",new cljs.core.Keyword(null,"value","value",1125876963),text], null)], null);
});
sablono.core.submit_button = sablono.core.wrap_attrs.call(null,sablono.core.submit_button8533);
/**
* Creates a form reset button.
*/
sablono.core.reset_button8534 = (function reset_button8534(text){return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",1114262332),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1017479852),"reset",new cljs.core.Keyword(null,"value","value",1125876963),text], null)], null);
});
sablono.core.reset_button = sablono.core.wrap_attrs.call(null,sablono.core.reset_button8534);
/**
* Create a form that points to a particular method and route.
* e.g. (form-to [:put "/post"]
* ...)
* @param {...*} var_args
*/
sablono.core.form_to8535 = (function() { 
var form_to8535__delegate = function (p__8536,body){var vec__8538 = p__8536;var method = cljs.core.nth.call(null,vec__8538,0,null);var action = cljs.core.nth.call(null,vec__8538,1,null);var method_str = clojure.string.upper_case.call(null,cljs.core.name.call(null,method));var action_uri = sablono.util.to_uri.call(null,action);return cljs.core.vec.call(null,cljs.core.concat.call(null,((cljs.core.contains_QMARK_.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"get","get",1014006472),null,new cljs.core.Keyword(null,"post","post",1017351186),null], null), null),method))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"form","form",1017053238),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"method","method",4231316563),method_str,new cljs.core.Keyword(null,"action","action",3885920680),action_uri], null)], null):new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"form","form",1017053238),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"method","method",4231316563),"POST",new cljs.core.Keyword(null,"action","action",3885920680),action_uri], null),sablono.core.hidden_field.call(null,"_method",method_str)], null)),body));
};
var form_to8535 = function (p__8536,var_args){
var body = null;if (arguments.length > 1) {
  body = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);} 
return form_to8535__delegate.call(this,p__8536,body);};
form_to8535.cljs$lang$maxFixedArity = 1;
form_to8535.cljs$lang$applyTo = (function (arglist__8539){
var p__8536 = cljs.core.first(arglist__8539);
var body = cljs.core.rest(arglist__8539);
return form_to8535__delegate(p__8536,body);
});
form_to8535.cljs$core$IFn$_invoke$arity$variadic = form_to8535__delegate;
return form_to8535;
})()
;
sablono.core.form_to = sablono.core.wrap_attrs.call(null,sablono.core.form_to8535);
