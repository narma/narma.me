// Compiled by ClojureScript 0.0-2227
goog.provide('chat.service');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('ajax.core');
goog.require('reagent.core');
goog.require('reagent.core');
goog.require('ajax.core');
chat.service.messages = reagent.core.atom.call(null,cljs.core.PersistentVector.EMPTY);
chat.service.channels = reagent.core.atom.call(null,cljs.core.PersistentVector.EMPTY);
chat.service.error_handler = (function error_handler(p__12544){var map__12546 = p__12544;var map__12546__$1 = ((cljs.core.seq_QMARK_.call(null,map__12546))?cljs.core.apply.call(null,cljs.core.hash_map,map__12546):map__12546);var status_text = cljs.core.get.call(null,map__12546__$1,new cljs.core.Keyword(null,"status-text","status-text",4371493274));var status = cljs.core.get.call(null,map__12546__$1,new cljs.core.Keyword(null,"status","status",4416389988));return console.log(("Something bad happened: "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(status)+" "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(status_text)));
});
chat.service.msg_send = (function msg_send(msg){return ajax.core.POST.call(null,"/messages/create",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"params","params",4313443576),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"msg","msg",1014012659),msg], null),new cljs.core.Keyword(null,"error-handler","error-handler",1866823671),chat.service.error_handler], null));
});
chat.service.load_history = (function load_history(){return ajax.core.GET.call(null,"/messages/list",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"error-handler","error-handler",1866823671),chat.service.error_handler,new cljs.core.Keyword(null,"handler","handler",1706707644),(function (p1__12547_SHARP_){var seq__12552 = cljs.core.seq.call(null,p1__12547_SHARP_);var chunk__12553 = null;var count__12554 = 0;var i__12555 = 0;while(true){
if((i__12555 < count__12554))
{var msg = cljs.core._nth.call(null,chunk__12553,i__12555);cljs.core.println.call(null,"import ",msg);
cljs.core.swap_BANG_.call(null,chat.service.messages,cljs.core.conj,msg);
{
var G__12556 = seq__12552;
var G__12557 = chunk__12553;
var G__12558 = count__12554;
var G__12559 = (i__12555 + 1);
seq__12552 = G__12556;
chunk__12553 = G__12557;
count__12554 = G__12558;
i__12555 = G__12559;
continue;
}
} else
{var temp__4126__auto__ = cljs.core.seq.call(null,seq__12552);if(temp__4126__auto__)
{var seq__12552__$1 = temp__4126__auto__;if(cljs.core.chunked_seq_QMARK_.call(null,seq__12552__$1))
{var c__4225__auto__ = cljs.core.chunk_first.call(null,seq__12552__$1);{
var G__12560 = cljs.core.chunk_rest.call(null,seq__12552__$1);
var G__12561 = c__4225__auto__;
var G__12562 = cljs.core.count.call(null,c__4225__auto__);
var G__12563 = 0;
seq__12552 = G__12560;
chunk__12553 = G__12561;
count__12554 = G__12562;
i__12555 = G__12563;
continue;
}
} else
{var msg = cljs.core.first.call(null,seq__12552__$1);cljs.core.println.call(null,"import ",msg);
cljs.core.swap_BANG_.call(null,chat.service.messages,cljs.core.conj,msg);
{
var G__12564 = cljs.core.next.call(null,seq__12552__$1);
var G__12565 = null;
var G__12566 = 0;
var G__12567 = 0;
seq__12552 = G__12564;
chunk__12553 = G__12565;
count__12554 = G__12566;
i__12555 = G__12567;
continue;
}
}
} else
{return null;
}
}
break;
}
})], null));
});
chat.service.pooling = (function pooling(){return ajax.core.GET.call(null,"/messages/poll",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"error-handler","error-handler",1866823671),chat.service.error_handler,new cljs.core.Keyword(null,"handler","handler",1706707644),(function (event){var G__12569_12570 = new cljs.core.Keyword(null,"type","type",1017479852).cljs$core$IFn$_invoke$arity$1(event);var caseval__12571;
switch (G__12569_12570){
case "message":
cljs.core.swap_BANG_.call(null,chat.service.messages,cljs.core.conj,event)
break;
default:
cljs.core.println.call(null,"unknown msg",event)
}
return pooling.call(null);
})], null));
});
chat.service.start = (function start(){return chat.service.pooling.call(null);
});
