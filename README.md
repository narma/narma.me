# site

## Simple chat system

Implemented: chat, history

TODO: channels, groups

## Usage 
1. Go to https://narma.me
2. Auth via one of social network or login with demo account
3. Chat!

## License

Copyright © 2014 Sergey Rublev

## Source
https://bitbucket.org/narma/narma.me

Distributed under the Eclipse Public License, the same as Clojure.
